---
draft: false
layout: blog
title: Surveillance 方法監測流行病趨勢資料
author: Hsien Ching Lo
date: 2019-04-25T01:24:00.876Z
categories:
  - R
  - Surveillance
tags:
  - R
  - Surveillance
Keywords:
  - R
  - Surveillance
---
在 R 的套件 [Surveillance](https://cran.r-project.org/web/packages/surveillance/surveillance.pdf)可作為判斷疫情的閾值，公式可參考 [Surveillance Algorithms](https://epub.ub.uni-muenchen.de/1791/1/paper_422.pdf)

<!--more-->



![](/images/uploads/2019-04-25-09-30-56-image-6.png)

原文說明：

> • "Bayes 1" reference values from 6 weeks. Alpha is fixed a t 0.05.
>
> • "Bayes 2" reference values from 6 weeks ago and 13 weeks of the previous year (symmetrical around the same week as the current one in the previous year). Alpha is fixed at 0.05.
>
> • "Bayes 3" 18 reference values. 9 from the year ago and 9 from two years ago (also symmetrical around the comparable week). Alpha is fixed at 0.05.

* 使用 Surveillance Algorithms 計算閾值
* 警戒閾值(Bayes 1)：參考近 6 週資料
* 警戒閾值(Bayes 2)：參考近 6 週與去年同期前後 6 週資料，共 19 個參考值
* 警戒閾值(Bayes 3)：參考前兩年同期前後 4 週資料，共 18 個參考值
