---
draft: false
layout: blog
title: Jenkins 建置於 Ubuntu
author: Hsien Ching Lo
date: 2019-04-30T01:26:34.739Z
categories:
  - Jenkins
tags:
  - Jenkins
Keywords:
  - Jenkins
---
# Jenkins 安裝 with docker

1. 建立資料層，docker 內的修改在 container 停止後是不會保留的，避免失去相關資料，需建立一個目錄來保存這些資料。(`-v /opt/jenkins:/var/jenkins_home`)
2. 設定 Host 跟 Container 的 Port 對應(`-p 8080:8080`)，如欲修改 Host Port 可參照(`-p [Host]:[Container]`)
3. 設定 Container 的名稱，方便識別，必須是唯一的，如果重覆必須先 rm 掉原本的 Container。(`--name jenkins`)


```bash
sudo apt-get install docker.io
sudo su
docker pull jenkins/jenkins:lts
mkdir /opt/jenkins
sudo chmod -R 777 /opt/jenkins/*
sudo docker run -d \
    -v /opt/jenkins:/var/jenkins_home \
    -p 8080:8080 \
    --name jenkins \
    jenkins/jenkins:lts
```

<!--more-->

進入 Jenkins 網址

![](/images/uploads/2019-04-30-09-30-16-image-3.png)

查詢密碼

```bash
vim /opt/jenkins/secrets/initialAdminPassword
```

登入之後如碰到 proxy 的問題(沒碰到可以 pass)

```bash
vim /opt/jenkins/hudson.model.UpdateCenter.xml
```

將 https 改成 http

```
<?xml version='1.1' encoding='UTF-8'?>
<sites>
  <site>
    <id>default</id>
    <url>http://updates.jenkins.io/update-center.json</url>
  </site>
</sites>
```

重啟

```bash
sudo docker restart jenkins
```

安裝 plugin(可選預設即可)

預設登入帳密為 admin 與 initialAdminPassword 的密碼

因資安可能有擋住部分網址，改以手動更新

```bash
https://updates.jenkins.io/download/plugins/ace-editor/
https://updates.jenkins.io/download/plugins/pipeline-stage-view/
https://updates.jenkins.io/download/plugins/workflow-aggregator/
```

安裝順序為 ace-editor -> pipeline-stage-view -> workflow-aggregator

# Add Service

container 存在 -> restart

container 不存在 -> run

```bash
sudo vim /usr/sbin/jenkins.sh
```

```
#!/bin/bash

if [ "$(sudo docker ps -f "name=jenkins" -aq)" == "" ]
  then 
    sudo docker run -d \
      -v /opt/jenkins:/var/jenkins_home \
      -p 8080:8080 \
      --name jenkins \
      jenkins/jenkins:lts
  else 
  	sudo docker restart jenkins
fi
```

```bash
sudo chmod +x /usr/sbin/jenkins.sh
sudo vim /etc/systemd/system/jenkins.service
```

```
[Unit]
Description=Jenkins
After=network.target

[Service]
User=root
Group=root
ExecStart=/usr/sbin/jenkins.sh
Restart=always
WorkingDirectory=/

[Install]
WantedBy=multi-user.target
```

```bash
sudo systemctl status jenkins.service
sudo systemctl start jenkins.service
sudo systemctl enable jenkins.service
```

# 其他 docker 用法

- - -

執行 docker 的 bash

```bash
sudo docker exec -it jenkins /bin/bash
```

如須清除/關閉 jenkins

```bash
sudo docker stop jenkins
sudo docker rm jenkins
```

```bash
sudo docker restart jenkins
```
