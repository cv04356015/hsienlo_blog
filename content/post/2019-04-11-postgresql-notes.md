---
draft: false
layout: blog
title: PostgreSQL 備份與還原
author: Hsien Ching Lo
date: 2019-04-11T07:21:00.096Z
categories:
  - PostgreSQL
  - 備份與還原
tags:
  - PostgreSQL
  - 備份與還原
Keywords:
  - PostgreSQL
  - 備份與還原
---
# 備份與還原

## 備份

要設定 PostgresSQL 排程備份，需先設定環境變數 `PSQLPWD`

### 環境變數設定

```bash
vim /etc/profile
```

末行輸入

```
export PSQLPWD='[password]'
```

在 `pg_dump` 指令之前放置 `PGPASSWORD=$PSQLPWD`，假如不是要設定排程的話，可手動輸入密碼，則不需要加入 `PGPASSWORD=$PSQLPWD`

```bash
PGPASSWORD=$PSQLPWD pg_dump -h 127.0.0.1 -p 5432 -U postgres -F c -b -v -f "/[備份位置]/[檔案名稱].bak" [資料庫名稱]
```

舉例

```bash
PGPASSWORD=$PSQLPWD pg_dump -h 127.0.0.1 -p 5432 -U postgres -F c -b -v -f "/media/sf_Eic03-2/RBackup/PostgreSQL/CWB_DATA.bak" CWB_DATA 
```

ref<sup>1</sup>: [SQL Dump](https://docs.postgresql.tw/server-administration/25.-bei-fen-ji-huan-yuan/25.1.-sql-dump)

## 還原

`PSQLPWD` 設定可參考[備份](#備份)所述

```bash
PGPASSWORD=$PSQLPWD pg_restore -h localhost -p 5432 -U postgres -d CWB_DATA /備份位置/[檔案名稱].bak
```

舉例

```bash
PGPASSWORD=$PSQLPWD pg_restore -h localhost -p 5432 -U postgres -d CWB_DATA /media/sf_Eic03-2/RBackup/PostgreSQL/CWB_DATA.bak
```

ref<sup>1</sup>: [pg_restore](https://www.postgresql.org/docs/10/app-pgrestore.html)
