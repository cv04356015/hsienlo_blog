---
draft: false
layout: blog
title: Shiny Server 建置
author: Hsien Ching Lo
date: 2019-05-14T02:50:37.566Z
categories:
  - Shiny
  - 系統建置
tags:
  - Shiny
  - 系統建置
Keywords:
  - Shiny
  - 系統建置
---
# Shiny Server 安裝建置

* [Install RStudio Shiny Server](https://www.rstudio.com/products/shiny/download-server/)


# Configuration

--- 

預設如下，這些設定可以修改。

* listen 3838;
* site_dir /srv/shiny-server;
* log_dir /var/log/shiny-server;

```bash
sudo vim /etc/nginx/sites-available/default
```

<!--more-->

```default
# Instruct Shiny Server to run applications as the user "shiny"
run_as shiny;

# Define a server that listens on port 3838
server {
  listen 3838;

  # Define a location at the base URL
  location / {

    # Host the directory of Shiny Apps stored in this directory
    site_dir /srv/shiny-server;

    # Log all Shiny output to files in this directory
    log_dir /var/log/shiny-server;

    # When a user visits the base URL rather than a particular application,
    # an index of the applications available in this directory will be shown.
    directory_index on;
  }
}
```

# Install Package

---

套件需安裝在 system 端，如安裝於 user 端 shiny 會看不到

```bash
sudo su -c "R -e \"install.packages('shiny',repos='https://cran.rstudio.com')\""
```

如果是 microsoft R

```bash
sudo su -c "R -e \"install.packages('shiny',repos='https://mran.revolutionanalytics.com')\""
```


## Server Configuration

---

* Nginx

```bash
sudo apt-get update
sudo apt-get install nginx
sudo vim /etc/nginx/sites-available/default
```

修改如下

```default
server {
    listen 80;
    server_name example.com/IP;

    charset     utf8;
    access_log    /var/log/nginx/access.log;

    # shiny server
    location /shiny/ {
        proxy_pass http://127.0.0.1:3838/;
    }
}
```

重啟

```bash
# reload the configuration
sudo systemctl reload nginx
```

# SSL HTTPS

---

* [shiny-server HTTPS 設定](https://jiankaiwang.gitbooks.io/itsys/content/r_architecture/shiny-server.html)


# 其他 Error

---

## shiny-server 無法增加專案

shiny-server 專案位置存放於 `/srv/shiny-server`

增加 shiny-server 權限

```bash
cd /srv/
sudo chmod 777 "shiny-server/"
```

## shiny-server 無法讀取 log

shiny-server log 位置存放於 `/var/log/shiny-server`

增加 shiny-server 權限

```bash
cd /var/log/
sudo chmod 777 "shiny-server/"
```

## Shiny 無法執行 terminal 指令

因為 Ubuntu 權限控管的關係，Shiny Server 的預設使用者是 Shiny，但是 Shiny 這個使用者是沒有直接權限可以執行 terminal，新增方法如下

```bash
vim /etc/sudoers
```

如果想讓 shiny 可以執行這兩個程式，即在後方新增 `/usr/sbin/useradd, /root/custom/crypt.o`，兩個程式是以逗號做區隔。

```sudoers
shiny   ALL = NOPASSWD: /usr/sbin/useradd, /root/custom/crypt.o
```
