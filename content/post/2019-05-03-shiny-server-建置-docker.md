---
draft: false
layout: blog
title: Shiny Server 建置(Docker)
author: Hsien Ching Lo
date: 2019-05-03T01:10:42.644Z
categories:
  - Docker
  - Shiny
tags:
  - Docker
  - Shiny
Keywords:
  - Docker
  - Shiny
---
```bash
docker pull rocker/shiny

docker run -itd \
    -p 3838:3838 \
    -v /srv/shinyapps/:/srv/shiny-server/ \
    -v /srv/shinylog/:/var/log/shiny-server/ \
    --name shiny \
    rocker/shiny
```

ref: [rocker/shiny](https://hub.docker.com/r/rocker/shiny/)

<!--more-->
