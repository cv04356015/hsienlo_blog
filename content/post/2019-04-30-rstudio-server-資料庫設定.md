---
draft: false
layout: blog
title: Rstudio Server 資料庫設定
author: Hsien Ching Lo
date: 2019-04-30T06:16:00.126Z
categories:
  - Rstudio
tags:
  - Rstudio
Keywords:
  - Rstudio
---
Rstudio Server ODBC 連線功能

![](/images/uploads/2019-04-30-14-17-10-image-1.png)

* 安裝 odbc 套件

```bash
sudo su -c "R -e \"install.packages('odbc', repos='https://cran.rstudio.com/')\""
```

* deb: unixodbc-dev (Debian, Ubuntu, etc)

* Terminal

<!--more-->

```bash
sudo apt-get install unixodbc-dev unixodbc-bin unixodbc
```

## 安裝其他 ODBC Driver

---

from : <http://db.rstudio.com/best-practices/drivers/>

* SQL Server 沒反應
* MySQL 有錯


```bash
# SQL Server ODBC Drivers (Free TDS)
# apt-get install tdsodbc

# PostgreSQL ODBC ODBC Drivers
apt-get install odbc-postgresql

# MySQL ODBC Drivers
apt-get install libmyodbc

# SQLite ODBC Drivers
apt-get install libsqliteodbc
```

## SQL Server ODBC Driver 補

- - -

the download url is :

[https://packages.microsoft.com/ubuntu/16.10/prod/pool/main/m/msodbcsql](https://docs.microsoft.com/zh-tw/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server)

```bash
sudo su 
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/16.10/prod.list > /etc/apt/sources.list.d/mssql-release.list
exit
sudo apt-get update
sudo ACCEPT_EULA=Y apt-get install msodbcsql
# optional: for bcp and sqlcmd
sudo ACCEPT_EULA=Y apt-get install mssql-tools
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
# optional: for unixODBC development headers
sudo apt-get install unixodbc-dev
```

## Setting on SQL Server Connect

- - -

ref: <http://thinkdatascience.com/how-to-install-odbc-driver-for-ubuntu-and-use-rodbc-package-in-r>

### Install FreeTDS ODBC driver

```
sudo apt-get install unixodbc unixodbc-dev freetds-dev tdsodbc
```

* 修改 DB Driver 內容


```bash
sudo vim /etc/odbcinst.ini
```

odbcinst.ini

```odbcinst.ini
[ODBC Driver 17 for SQL Server]
Description=Microsoft ODBC Driver 17 for SQL Server
Driver=/opt/microsoft/msodbcsql/lib64/libmsodbcsql-17.0.so.1.1
UsageCount=1
```

* 設定可連接的資料庫


```bash
sudo vim /etc/odbc.ini
```

odbc.ini

```odbc.ini
[RODS_ODBC]
Driver=/usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
Trace=No
Server=192.168.170.70\mssqlserver2
TDS_Version=7.0
# Database=emg
```

* 設定 TDS


```bash
sudo vim /usr/share/freetds/freetds.conf
```

freetds.conf

```freetds.conf
[RODS_ODBC]
        host = 192.168.170.70
        instance = mssqlserver2
        port = 1433
        tds version = 7.0
```

instance 要獨立出來

* 測試是否可以連線

如果有特殊符號要引號

```bash
isql -v RODS_ODBC <user> "<pwd>"
```

輸出

```
+---------------------------------------+
| Connected!                            |
|                                       |
| sql-statement                         |
| help [tablename]                      |
| quit                                  |
|                                       |
+---------------------------------------+
SQL>
```

同樣，可以另外建立一個 FreeTDS Driver 作為資料庫的指引

ref: <https://stackoverflow.com/questions/8511369/adaptive-server-is-unavailable-or-does-not-exist-error-connecting-to-sql-serve>

odbcinst.ini

```odbcinst.ini
[FreeTDS]
Description = FreeTDS Driver v0.91
Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
Setup = /usr/lib/x86_64-linux-gnu/odbc/libtdsS.so
fileusage=1
dontdlclose=1
UsageCount=1
odbc.ini:

[test]
Driver = FreeTDS
Description = My Test Server
Trace = No
#TraceFile = /tmp/sql.log
ServerName = mssql
#Port = 1433
instance = SQLEXPRESS
Database = usedbname
TDS_Version = 4.2
FreeTDS.conf:

[mssql]
host = hostnameOrIP
instance = SQLEXPRESS
#Port = 1433
tds version = 4.2
```

### 有關 TDS Version 版本選項

- - -

ref: <https://msdn.microsoft.com/en-us/library/dd339982.aspx>

![](/images/uploads/2019-04-30-14-17-10-image-2.png)

### 有關 odbc.ini 設定與參數說明

- - -

ref: <https://www.easysoft.com/products/data_access/odbc-sql-server-driver/manual/configuration.html>

```output
 [SQL Server]

 Driver = Easysoft ODBC-SQL Server

 # To connect to the default instance, omit \my_instance_name.

 Server = my_sqlserver_hostname\my_instance_name

 User = my_domain\my_domain_user

 Password = my_password

 If the SQL Server Browser or listener service is not in use at your site and you want to connect to an instance that is not listening on the default TCP port (1433), you also need to specify the port: For example, to connect to a SQL Server instance that is listening on port 1500, add this entry:

 Port = 1500
```

### 有關 freetds.conf 設定與參數說明

- - -

ref:  <http://www.freetds.org/userguide/freetdsconf.htm>

freetds.conf

```freetds.conf
[global]
tds version = 4.2
[myserver]
host = ntbox.mydomain.com
port = 1433
[myserver2]
host = unixbox.mydomain.com
port = 4000
tds version = 5.0
[myserver3]
host = instancebox.mydomain.com
instance = foo
tds version = 7.1
```

## Setting on Oracle Connect

- - -

主要為參考這篇，不同的是我安裝 12.2 版的 client 端

ref: http://www.xuefliang.org/2015/12/install-rodbc-with-oracle-12c-odbc-in.html

其他參考：

ref: http://docs.adaptivecomputing.com/9-1-0/MWS/Content/topics/moabWorkloadManager/topics/databases/oracle.html

ref: https://help.ubuntu.com/community/Oracle%20Instant%20Client

1、安装 Oracle Instant Cilent

```bash
sudo su
apt install alien
sudo apt-get install libaio1
alien -i oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
alien -i oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm
alien -i oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
alien -i oracle-instantclient12.2-odbc-12.2.0.1.0-2.x86_64.rpm
```

Instant Client Home page: <http://www.oracle.com/technetwork/database/features/instant-client/index-097480.html>

Download page: <http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html> (Linux 64 bit)

2、Conf

```bash
sudo vim /etc/ld.so.conf.d/oracle.conf
```

oracle.conf

```oracle.conf
/usr/lib/oracle/12.2/client64/lib
```

接著輸入

```bash
sudo ldconfig
```

> ldconfig命令的用途主要是在默認搜尋目錄/lib和/usr/lib以及動態庫配置文件內所列的目錄下，搜索出可共享的動態鏈接庫（格式如lib_.so_）,進而創建出動態裝入程序(ld .so)所需的連接和緩存文件。
>
> ref: http://man.linuxde.net/ldconfig

```bash
sudo vim /etc/profile.d/oracle.sh
```

添加

```bash
export ORACLE_HOME= /usr/lib/oracle/12.2/client64
```

3、安裝unixodbc 

```bash
sudo apt-get install unixodbc
```

4、配置連接

```bash
sudo vim /etc/odbc.ini
```

odbc.ini

```odbc.ini
[DW_ODBC]
Application Attributes = T
Attributes = W
BatchAutocommitMode = IfAllSuccessful
CloseCursor = F
DisableDPM = F
DisableMTS = T
Driver = Oracle
EXECSchemaOpt =
EXECSyntax = T
Failover = T
FailoverDelay = 10
FailoverRetryCount = 10
FetchBufferSize = 64000
ForceWCHAR = F
Lobs = T
Longs = T
MetadataIdDefault = F
QueryTimeout = T
ResultSets = T
ServerName = //192.168.170.54:1561/DW
SQLGetData extensions = F
Translation DLL =
UserName = "sas"
Password = "ueCr5brAD6u4rAs62t9a"
```

* 連線 54 或 52 任一都可以，兩台 oracle 是在另一台壞掉後可方便運作，但目前沒看到可以同時寫的方法

參照檔為 `tnsnames.ora`

* ServerName = //\[host]:\[port]/\[service_name]


```tnsnames.ora
DW =
  (description =
    (address = (protocol = tcp)(host = 192.168.170.54)(port = 1561))
    (address = (protocol = tcp)(host = 192.168.170.52)(port = 1561))
    (load_balance = yes)
    (connect_data =
      (server = dedicated)
      (service_name = DW)
      (failover_mode =
        (type = select)
        (method = basic)
        (retries = 10)
        (delay = 3)
      )
    )
  )
```

```bash
sudo vim /etc/odbcinst.ini 
```

odbcinst.ini 

```odbcinst.ini 
[Oracle]
Description = Oracle ODBC Connection
Driver = /usr/lib/oracle/12.2/client64/lib/libsqora.so.12.1
Setup =
FileUsage =
CPTimeout =
CPReuse =
```

5、測試

isql DW_ODBC sas ueCr5brAD6u4rAs62t9a 

## Setting postgresql connect

```
PostgreSQL Unicode
```
