---
draft: false
layout: blog
title: Python Notes
author: Hsien Ching Lo
date: 2019-04-30T02:51:44.513Z
categories:
  - Python
tags:
  - Python
Keywords:
  - Python
---
在Python中，這種一邊循環一邊計算的機制，稱為生成器 (Generator)

# 建立 dataFrame 與資料合併

```python
import pandas as pd

lkey = ["foo", "bar", "baz", "foo"]
value = [1,2,3,4]
A = {"lkey": lkey, "value": value}
A = pd.DataFrame(A)
rkey = ["foo", "bar", "qux", "bar"]
value = [5,6,7,8]
B = {"rkey": rkey, "value": value}
B = pd.DataFrame(B)

A.merge(B, left_on='lkey', right_on='rkey', how='outer')
```

# History 查看先前執行的程式碼

<!--more-->
