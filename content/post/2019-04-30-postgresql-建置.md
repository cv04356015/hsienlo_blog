---
draft: false
layout: blog
title: PostgreSQL 建置
author: Hsien Ching Lo
date: 2019-04-30T02:42:07.162Z
categories:
  - PostgreSQL
tags:
  - PostgreSQL
Keywords:
  - PostgreSQL
---
Ubuntu 預設的 Repository 安裝

```
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
```

# 安裝最新版本 PostgreSQL

- - -

最新版 postgresql 安裝方法

<https://www.postgresql.org/download/linux/ubuntu/>

* 查看 Ubuntu 版本


```
lsb_release -a
```

```
No LSB modules are available.
```

```

Distributor ID: Ubuntu
Description:    Ubuntu 16.04.3 LTS
Release:        16.04
Codename:       xenial
```

<!--more-->

* 選擇 Ubuntu 版本

![](/images/uploads/2019-04-30-10-43-16-image-0.png)

* Create the file /etc/apt/sources.list.d/pgdg.list, and add a line for the repository


```
deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main
```

* Import the repository signing key, and update the package lists


```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
```

* run


```
sudo -u postgres psql
postgres=# \q
```

* 設定 postgres 帳號的密碼

postgres 為管理員帳號

```
sudo -u postgres psql
postgres=# alter user postgres password '1qaz@WSX';
```

# 提供非本機端連線，需新增設定

* main configuration


```
# the main configurated file
sudo vim /etc/postgresql/(version)/main/postgresql.conf
```

* allow all IP connection configuration


```
# the main configurated file
sudo vim /etc/postgresql/9.5/main/postgresql.conf
```

```
# allow all IP
listen_addresses = '*'
```

* edit the server configurated file


```
sudo vim /etc/postgresql/9.5/main/pg_hba.conf
```

```
# add the following conf to allow all IP
host    all             all             0.0.0.0/0            md5
```

reload the configuration by restarting the service

```
sudo systemctl restart postgresql.service
```

* If there is authentication failed for user "postgres", you would edit the configuration in pg_hba.conf.


```
sudo vim /etc/postgresql/9.5/main/pg_hba.conf
```

```
# Database administrative login by Unix domain socket
local   all             postgres                                md5
```

reload the configuration by restarting the service

```
sudo systemctl restart postgresql.service
```

# 建立 Postgres 使用者

非本機使用者

```
# create a user named postgreuser
# -S : User will not be a superuser
# -D : User cannot create databases
# -R : User cannot create other roles (users)
# -P : Prompt to create a password for the user
sudo -u postgres createuser -S -D -R -P example

# create a database (named example, exampledb2) and assign it to the specific user
# -O : Owner - the user that owns the database
# -E : Encoding - almost always UTF8
sudo -u postgres createdb -O example example -E utf-8
sudo -u postgres createdb -O example exampledb2 -E utf-8
```

If there is authentication failed for user "example", you would edit the configuration in pg_hba.conf.

```
sudo vim /etc/postgresql/9.5/main/pg_hba.conf
```

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
host    all             all             0.0.0.0/0               md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
```

reload the configuration by restarting the service

```
sudo systemctl restart postgresql.service
```

# 連結 Foreign Data Wrapper

前言：

* 無需設定 listener.ora
* 需先行裝好 oracle client

相關連結：

* [oracle_fdw](https://github.com/laurenz/oracle_fdw)
* [在 PostgreSQL 透過 Foreign Data Wrapper 與 Oracle 資料庫交換資料](http://ravenonhill.blogspot.tw/2016/06/postgresql-foreign-data-wrapper-oracle.html)
* [PostgreSQL Oracle FDW… in 8i?!](https://www.keithf4.com/oracle_fdw/)

```bash
cd /tmp/
git clone https://github.com/laurenz/oracle_fdw.git
cd oracle_fdw/
make
make install
sudo -u postgres psql
postgres=# CREATE EXTENSION oracle_fdw;
postgres=# SELECT name FROM pg_available_extensions WHERE name LIKE '%fdw';
# 建立 Foreign Data Wrappers
postgres=# CREATE SERVER oracle_server FOREIGN DATA WRAPPER oracle_fdw OPTIONS (dbserver '//192.168.170.54:1561/DW');
postgres=# CREATE USER MAPPING FOR CURRENT_USER SERVER oracle_server OPTIONS (user 'sas', password 'ueCr5brAD6u4rAs62t9a');
# 查看 CURRENT_USER
postgres=# SELECT CURRENT_USER;
postgres=# CREATE FOREIGN TABLE oracle_table (REPORT text) SERVER oracle_server OPTIONS (schema 'CDCDW', table 'V_DOH_DEATH_ALL');
postgres=# SELECT * FROM public.oracle_table LIMIT 100;
```

make install 可能會遇到的問題

```
Makefile:7: /usr/lib/postgresql/9.3/lib/pgxs/src/makefiles/pgxs.mk: No such file or directory  
make: *** No rule to make target `/usr/lib/postgresql/9.3/lib/pgxs/src/makefiles/pgxs.mk'.  Stop. 
```

出現這個問題之後，根據google的結果發現是少安裝了兩個開發包，於是直接常規用戶（非postgres用戶非root用戶）下直接sudo安裝：

```
sudo apt-get install postgresql-server-dev-all  
sudo apt-get install postgresql-common  
```

* add ld.so.conf


```
sudo vim /etc/ld.so.conf
```

```
include /etc/ld.so.conf.d/*.conf
/usr/lib/oracle/12.2/client64/lib/
```

* check CURRENT_USER

```psql
SELECT CURRENT_USER;
```

* 連線設定


```
CREATE SERVER oracle_server FOREIGN DATA WRAPPER oracle_fdw OPTIONS (dbserver '//192.168.170.54:1561/DW');
CREATE USER MAPPING FOR CURRENT_USER SERVER oracle_server OPTIONS (user 'sas', password 'ueCr5brAD6u4rAs62t9a');
```

* 清除 Foreign Data Wrapper 安裝


```
postgres=# DROP SERVER oracledb CASCADE;
postgres=# DROP EXTENSION oracle_fdw
```

* Login the server


```
psql -h localhost -p 5432 -U postgres
```

* Database Operations


```
postgres=# /* List Databases */
postgres=# \list
postgres=# \l
postgres=# /* Connect to the specific database */
postgres=# \connect <dbname>
postgres=# /* list table */
postgres=# \dt
postgres=# /* table info */
postgres=# \d+ <table>
```

# postgresql 記憶體優化

```
sudo vim /etc/postgresql/(version)/main/postgresql.conf
```

> shared_buffers：這是最重要的參數，postgresql通過shared_buffers和內核和磁片打交道，因此應該儘量大，讓更多的資料緩存在shared_buffers中。通常設置為實際RAM的10％是合理的，比如50000(400M)
>
> effective_cache_size：是postgresql能夠使用的最大緩存，這個數字對於獨立的pgsql伺服器而言應該足夠大，比如4G的記憶體，可以設置為3.5G(437500)

ref:[postgresql 記憶體優化](http://myblog-maurice.blogspot.com/2011/12/postgresql_1627.html)
