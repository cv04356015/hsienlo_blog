---
draft: false
layout: blog
title: Python 利用 selenium 截取網頁資料
author: Hsien Ching Lo
date: 2019-04-24T03:06:54.506Z
categories:
  - python
  - selenium
tags:
  - python
  - selenium
Keywords:
  - python
  - selenium
---
# 環境安裝

> 系統環境: Ubuntu server 18.04 LTS

```bash
# 安裝 chromedriver
sudo cp chromedriver /usr/local/bin/chromedriver
sudo chmod +x /usr/local/bin/chromedriver
# 安裝套件
pip3 install selenium
```

嘗試連接 webdriver 發生錯誤如下

```
WebDriverException: Message: unknown error: Chrome failed to start: exited abnormally
```

<!--more-->

需增加 chrome_options 指令

```python
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
path = '/usr/local/bin/chromedriver'
chrome_options = Options()
chrome_options.add_argument("--headless")
driver = webdriver.Chrome(executable_path = path, chrome_options = chrome_options)
driver.get("http://dwweb.cdc.gov.tw/dwweb/CDCDWQW0010R.aspx")
```

# selenium 指令

列出網頁內容 <font color="red">其中 text 不需要括弧()</font>

```
driver.find_element_by_css_selector(".h3").text
```

## 操控網頁元素

ref<sup>1</sup>: [Python 學習筆記 : Selenium 模組瀏覽器自動化測試 (二)](http://yhhuang1966.blogspot.com/2018/05/python-selenium_27.html)
