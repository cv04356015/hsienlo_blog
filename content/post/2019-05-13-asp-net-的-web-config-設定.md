---
draft: false
layout: blog
title: ASP.NET 的 Web.config 設定
author: Hsien Ching Lo
date: 2019-05-13T09:09:01.551Z
categories:
  - ASP.NET
tags:
  - ASP.NET
Keywords:
  - ASP.NET
---
如果希望所有文件皆可下載

```xml
<?xml version="1.0" encoding="UTF-8"?>
 <configuration>
     <system.webServer>
         <staticContent>
             <mimeMap fileExtension="." mimeType="application/octet-stream" />
         </staticContent>
     </system.webServer>
 </configuration>
```

<!--more-->
