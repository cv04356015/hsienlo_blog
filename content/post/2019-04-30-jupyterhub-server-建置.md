---
draft: false
layout: blog
title: JupyterHub Server 建置
author: Hsien Ching Lo
date: 2019-04-30T02:27:54.409Z
categories:
  - Jupyter
tags:
  - Jupyter
Keywords:
  - Jupyter
---
# JupyterHub 安裝建置

Reference

* [https://jupyterhub.readthedocs.io/en/latest/quickstart.html](https://jupyterhub.readthedocs.io/en/latest/quickstart.html)
* [https://media.readthedocs.org/pdf/jupyterhub/latest/jupyterhub.pdf](https://media.readthedocs.org/pdf/jupyterhub/latest/jupyterhub.pdf)

## 前言

---

* anaconda 安裝在 root 下面，而不是使用者
* JupyterHub 需要的東西我裝在 root 使用者目錄下，而非管理員 (rserver) 目錄下

<!--more-->

## Install

---

* Install **nodejs**
* 有一個設定會用到 nodejs-legacy

```
$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
$ sudo apt-get install -y nodejs
$ sudo apt-get install npm nodejs-legacy
```

* Install the package.

```
# install jupyterhub and nodejs-based http-proxy
$ python3 -m pip install jupyterhub
$ sudo npm install -g configurable-http-proxy
$ python3 -m pip install notebook  # needed if running the notebook servers locally

# verify the installation
$ jupyterhub -h
$ configurable-http-proxy -h
```

## Configuration

---

```
$ mkdir /rppt/.jupyterhub/
$ cd /root/.jupyterhub/

# generate a configuration file
$ jupyterhub --generate-config

# the default configuration path
$ vim /root/.jupyterhub/jupyterhub_config.py
```

修改如下

```
# [optional] Set options for certfile, ip, password, and toggle off browser auto-opening
# c.JupyterHub.ssl_cert = u'/etc/letsencrypt/live/example.com/cert.pem'
# c.JupyterHub.ssl_key = u'/etc/letsencrypt/live/example.com/privkey.pem'

## The public facing ip of the whole application (the proxy)
c.JupyterHub.ip = '0.0.0.0'

## The public facing port of the proxy
c.JupyterHub.port = 8000

## File in which to store the cookie secret.
c.JupyterHub.cookie_secret_file = '/root/.jupyterhub/jupyterhub_cookie_secret'

## url for the database. e.g. `sqlite:///jupyterhub.sqlite`
c.JupyterHub.db_url = 'sqlite:///root/.jupyterhub/jupyterhub.sqlite'

```

## Start the jupyterhub server.

---

```
$ sudo -s
# jupyterhub -f /root/.jupyterhub/jupyterhub_config.py
```

## Establish the service.\(Anaconda\)

---

```
# create a new service
$ sudo vim /etc/systemd/system/jupyterhubserver.service
```

內容如下

```
[Unit]
Description=Jupyterhub

After=syslog.target network.target


[Service]
User=root

Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/home/(user)/anaconda3/bin"
ExecStart=/home/(user)/anaconda3/bin/jupyterhub -f /home/(user)/.jupyterhub/jupyterhub_config.py


[Install]
WantedBy=multi-user.target
```

root 設定版本

```
[Unit]
Description=Jupyterhub
After=syslog.target network.target

[Service]
User=root
Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/anaconda3/bin"
ExecStart=/root/anaconda3/bin/jupyterhub -f /root/.jupyterhub/jupyterhub_config.py

[Install]
WantedBy=multi-user.target
```

* Add the service at server restart

```
$ sudo systemctl enable jupyterhubserver.service
```

* Start the service.

```
$ sudo systemctl start jupyterhubserver.service
$ sudo systemctl status jupyterhubserver.service
$ sudo systemctl enable jupyterhubserver.service
```

* Surf the service.

```
<!-- surf the link -->
http(s)://example.com:8000/
```

## 加入開機選單

---

from: [https://github.com/jupyterhub/jupyterhub/wiki/Run-jupyterhub-as-a-system-service](https://github.com/jupyterhub/jupyterhub/wiki/Run-jupyterhub-as-a-system-service)

Save [https://gist.github.com/lambdalisue/f01c5a65e81100356379](https://gist.githubusercontent.com/lambdalisue/f01c5a65e81100356379/raw/ecf427429f07a6c2d6c5c42198cc58d4e332b425/jupyterhub) as `/etc/init.d/jupyterhub`and follow

```
$ sudo chmod +x /etc/init.d/jupyterhub
# Create a default config to /etc/jupyterhub/jupyterhub_config.py
$ sudo jupyterhub --generate-config -f /etc/jupyterhub/jupyterhub_config.py
# Start jupyterhub
$ sudo service jupyterhub start
# Stop jupyterhub
$ sudo service jupyterhub stop
# Start jupyterhub on boot
$ sudo update-rc.d jupyterhub defaults
# Or use rcconf to manage services http://manpages.ubuntu.com/manpages/natty/man8/rcconf.8.html
$ sudo rcconf
# Stop jupyterhub on boot
$ sudo update-rc.d jupyterhub defaults
```

### vim error E212: Can't open file for writing

---

form :  [http://oscarguo.blogspot.tw/2016/09/vim-error-e212-cant-open-file-for.html](http://oscarguo.blogspot.tw/2016/09/vim-error-e212-cant-open-file-for.html)

以前在Linux環境下用root習慣了，要做任何事情都沒有問題，

現在用一般帳號登入就開始遇到一些之前沒遇到過的問題。

這次在用vim要存檔案時出現錯誤訊息 E212: Can't open file for writing

由辜狗大神發現如果是在vim要存檔時出現E212錯誤訊息是要透過以下的指令存檔

:w !sudo tee %

然後選L，就可以存檔了~~~

最保險的方式就是每次要用vim時在下命令時都要用sudo，如下

sudo vim FILE\_NAME

## Server Configuration

---

* Nginx

How To Install Nginx on Ubuntu 16.04

* form : [https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

```
$ sudo vim /etc/nginx/sites-available/default
```

內容

```
server {
    listen 80;
    server_name example.com;

    charset     utf8;
    access_log    /var/log/nginx/access.log;

    # jupyter portal
    location /jupyter {
        rewrite /jupyter /hub/ redirect;
    }    

    # for login
    location /hub/ {
        proxy_pass http://127.0.0.1:8000/hub/;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # web socket
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    # for main service
    location /user/ {
        proxy_pass http://127.0.0.1:8000/user/;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # web socket
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }
}
```

```
# reload the configuration
$ sudo systemctl reload nginx
```
