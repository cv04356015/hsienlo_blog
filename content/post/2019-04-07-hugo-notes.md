---
draft: false
layout: blog
title: Hugo Notes
author: Hsien Ching Lo
date: 2019-04-07T07:12:49.867Z
categories:
  - Hugo
tags:
  - Hugo
Keywords:
  - Hugo
---
[Hugo](https://gohugo.io/) 是一個以 Go 語言生成的靜態網站，因此建置速度非常的快速，在[staticgen](https://www.staticgen.com/) 靜態生成器的排行目前第三。[Hugo Github](https://github.com/gohugoio/hugo)

# 架站

可透過官方文件 [Quick Start](https://gohugo.io/getting-started/quick-start/) 建立一個簡單的 Hugo 網站，亦可透過 Gitlab 提供的模板進行的架設，Gitlab 提供了兩個 Hugo 模版供使用

## Gitlab 套用 Pages/Hugo 模組

透過 Gitlab 本身的 CI/CD 去部屬靜態網站

```
New project -> Creat from template -> Pages/Hugo -> Use template
```

> 可選擇不使用模版，透過 Quick Start 建置完成後，在加上 `.gitlab-ci.yml` 文件即可

創建完成後須做簡易的修改才會開始動作

進入 `config.toml` 編輯 (Edit) 將

<!--more-->

```
baseurl = "https://pages.gitlab.io/hugo/"
```

改為

```
baseurl = "http://cv04356015.gitlab.io/hugo-template-demo/"
```

進入 CI/CD -> Jobs 

如看到

```bash
Updating/initializing submodules recursively...
$ hugo
Building sites … 
                   | EN  
+------------------+----+
  Pages            | 35  
  Paginator pages  |  2  
  Non-page files   |  0  
  Static files     | 23  
  Processed images |  0  
  Aliases          |  9  
  Sitemaps         |  1  
  Cleaned          |  0  

Total in 241 ms
Uploading artifacts...
public: found 115 matching files                   
Uploading artifacts to coordinator... ok            id=192169653 responseStatus=201 Created token=ut11rh3Q
Job succeeded
```

表示已經部屬成功了

雖然看到成功，但網頁不會那麼快出來(第一次會慢點)

## Gitlab 套用 Netlify/Hugo 模組

* 透過 Netlify 進行部屬
* [headlesscms](https://headlesscms.org/) 排名前三的編輯器
* [Netlify CMS](https://www.netlifycms.org/) 不需安裝即可直接在網頁上進行編輯
* 一鍵部屬 [Netlify CMS](https://github.com/netlify-templates/one-click-hugo-cms) 功能

以下是完成 Netlify CMS 的步驟

Step1. Gitlab 設定

```
New project -> Creat from template -> Netlify/Hugo -> Use template
```

Step2. [Netlify](https://www.netlify.com/) 設定

```
New site form Git -> 在 Continuous Deployment 選擇 Gitlab -> 再選擇剛剛創建的 repos 其他接不需做任何的修改
```

等待網址自動產生之後，可自行視需求新增 domain

進入 `config.toml` 編輯 (Edit) 將

<!--more-->

```
baseurl = "https://pages.gitlab.io/hugo/"
```

改為

```
baseurl = "https://netlify-hugo-template-demo.netlify.com/"
```

Step3. Netlify CMS 設定

進入 `static`
 資料夾新增 `admin` 與 `images` 兩個資料夾

進入 `admin` 資料夾新增兩個檔案 `index.html` 與 `config.yml`

index.html 內容如下

```html
<!doctype html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Content Manager</title>
</head>
<body>
  <!-- Include the script that builds the page and powers Netlify CMS -->
  <script src="https://unpkg.com/netlify-cms@^2.0.0/dist/netlify-cms.js"></script>
</body>
</html>
```

config.yml 內容如下：

backend 包含
* name 是登入的方式，我這邊選擇 gitlab
* repo 為你的 GitLab repository
* auth_type
* app_id 要進入 User Settings -> Applications 去新增，如下圖

ref<sup>1</sup>: [GitLab as OAuth2 authentication service provider](https://docs.gitlab.com/ee/integration/oauth_provider.html#adding-an-application-through-the-profile)

ref<sup>2</sup>: [Netlify CMS Add to Your Site](https://www.netlifycms.org/docs/add-to-your-site/)

ref<sup>3</sup>: [Netlify CMS Configuration Options](https://www.netlifycms.org/docs/configuration-options/)

![](/images/uploads/2019-04-08-15-21-10-image-16.png)

Redirect URI 為 Netlify 網址 + /admin/

collections 的寫法可參考

1. [Hugo + gitlab + netlifyのブログにNetlify CMSを設定する方法](https://cloudlance-motio.work/post/netlify-cms-setting/)
2. 每個可填入的參數都具有一定的型態(Boolean、Date、DateTime、File、Hidden、Image、List、Map、Markdown ... 等)，透過 [widgets](https://www.netlifycms.org/docs/widgets/) 來編譯

```yml
backend:
  name: gitlab
  repo: cv04356015/netlify-hugo-template-demo # Path to your GitLab repository
  auth_type: implicit # Required for implicit grant
  app_id: [app_id]  # Application ID from your GitLab settings

# This line should *not* be indented
# These lines should *not* be indented
media_folder: "static/images/uploads" # Media files will be stored in the repo under static/images/uploads
public_folder: "/images/uploads" # The src attribute for uploaded media will begin with /images/uploads

collections:
  - name: "post" # Used in routes, e.g., /admin/collections/blog
    label: "post" # Used in the UI
    folder: "content/post/" # The path to the folder where the documents are stored
    create: true # Allow users to create new documents in this collection
    slug: "{{year}}-{{month}}-{{day}}-{{slug}}" # Filename template, e.g., YYYY-MM-DD-title.md
    fields: # The fields for each document, usually in front matter
      - {label: "Draft", name: "draft", widget: "hidden", default: false}
      - {label: "Layout", name: "layout", widget: "hidden", default: "blog"}
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Author", name: "author", widget: "string", default: "Hsien Ching Lo"}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Body", name: "body", widget: "markdown"}
      - {label: "Categories", name: "categories", widget: "list", required: false}
      - {label: "Tags", name: "tags", widget: "list", required: false}
      - {label: "Keywords", name: "Keywords", widget: "list", required: false}
      - {label: "Image", name: "image", widget: "image", required: false}
  - name: "index"
    label: "index"
    files:
      - file: "content/_index.md"
        label: "index"
        name: "index"
        fields:
          - {label: "Layout", name: "layout", widget: "hidden", default: "blog"}
          - {label: "Title", name: "title", widget: "string"}
          - {label: "Publish Date", name: "date", widget: "datetime"}
          - {label: "Body", name: "body", widget: "markdown"}
          - {label: "Image", name: "image", widget: "image", required: false}
```

進入 `images` 資料夾新增 `uploads` 資料夾

# themes

[Hugo Themes](https://themes.gohugo.io/)

* API
  * [hugo-material-docs](https://github.com/digitalcraftsman/hugo-material-docs)
* Blog
  * [hugo-w3-simple](https://github.com/jesselau76/hugo-w3-simple) 有不錯的圖片展示功能。
  * https://linuxer.io/about/
* Design
  * [meghna-hugo](https://themes.gohugo.io/theme/meghna-hugo) 設計感很厲害，很像一個公司的首頁。


# 內容管理

Hugo 可以解析 Markdown 文件供網頁所取用

{{ .TableOfContents }}

# 網站評論功能

ref<sup>1</sup>: [HOW TO INSTALL DISQUS ON HUGO?](https://notes.peter-baumgartner.net/tutorial/how-to-install-disqus-on-hugo/)

ref<sup>2</sup>: [Hugo 引入 Gitment](https://jimmysong.io/hugo-handbook/steps/comment-plugin.html)

# Markdown 新增數學符號顯示

ref<sup>1</sup>: [在Hugo中使用MathJax](http://note.qidong.name/2018/03/hugo-mathjax/)

進入 blog 的 `themes\hugo-theme-flybird\layouts\partials\footer.html`

```html
<!--匯入MathJax-->
<script type="text/javascript"
        async
        src = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
MathJax.Hub.Config({
  tex2jax: {
    inlineMath: [['$','$'], ['\\(','\\)']],
    displayMath: [['$$','$$'], ['\[\[','\]\]']],
    processEscapes: true,
    processEnvironments: true,
    skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'],
    TeX: { equationNumbers: { autoNumber: "AMS" },
         extensions: ["AMSmath.js", "AMSsymbols.js"] }
  }
});

MathJax.Hub.Queue(function() {
    // Fix <code> tags after MathJax finishes running. This is a
    // hack to overcome a shortcoming of Markdown. Discussion at
    // https://github.com/mojombo/jekyll/issues/199
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>

<style>
code.has-jax {
    font: inherit;
    font-size: 100%;
    background: inherit;
    border: inherit;
    color: #515151;
}
</style>
```

# 參考文獻

以下是近幾天搜尋出來的教學文件

1. [静态网站构建手册-使用Hugo构建个人博客](https://jimmysong.io/hugo-handbook/)
2. [markdown及latex语法](https://chen-feiyang.github.io/post/markdown%E5%8F%8Alatex%E8%AF%AD%E6%B3%95/)
3. [希腊字母表 Markdown版](https://zhuanlan.zhihu.com/p/32322376)
