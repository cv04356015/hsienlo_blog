---
draft: false
layout: blog
title: Rstudio Server 建置
author: Hsien Ching Lo
date: 2019-04-30T05:58:50.074Z
categories:
  - R
  - Rstudio
  - 系統建置
tags:
  - R
  - Rstudio
  - 系統建置
Keywords:
  - R
  - Rstudio
  - 系統建置
---
# Installiation 

--- 

```
# update the os system and others tools
sudo apt-get update
sudo apt-get upgrade

# install R-core
cd /usr/src
sudo sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
sudo apt-get update
sudo apt-get install r-base

# install rstudio server
sudo apt-get install gdebi-core
wget https://download2.rstudio.org/rstudio-server-1.1.419-amd64.deb
sudo gdebi rstudio-server-1.1.419-amd64.deb

# verify the installiation
sudo rstudio-server verify-installation

# start the rstudio server service
sudo systemctl start rstudio-server.service
sudo systemctl status rstudio-server.service
sudo systemctl enable rstudio-server.service
```

<!--more-->

## gpg: keyserver receive failed: keyserver error

--- 

錯誤原因：防火牆擋住其他 port，需使用 80

```
rserver@rserver:/usr/srcsudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
Executing: /tmp/tmp.Ah2GN5ef7C/gpg.1.sh --keyserver
keyserver.ubuntu.com
--recv-keys
E298A3A825C0D65DFD57CBB651716619E084DAB9
gpg: requesting key E084DAB9 from hkp server keyserver.ubuntu.com
gpg: keyserver timed out
gpg: keyserver receive failed: keyserver error
```

更改 keyserver

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
```

## E: Failed to fetch xxx server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none

--- 

錯誤原因：防火牆趨勢認證(IWSVA)擋住

```
rserver@rserver:/usr/srcsudo apt-get install r-base

...

W: http://us.archive.ubuntu.com/ubuntu/pool/main/f/fontconfig/fontconfig_2.11.94-0ubuntu1.1_amd64.deb: Automatically disabled Acquire::http::Pipeline-Depth due to incorrect response from server/proxy. (man 5 apt.conf)
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-base-core_3.4.3-1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-boot_1.3-20-1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-cluster_2.0.6-2xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-foreign_0.8.69-1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-mass_7.3-48-1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-kernsmooth_2.23-15-3xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-lattice_0.20-35-1cran1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-nlme_3.1.131-3xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-matrix_1.2-11-1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-mgcv_1.8-23-1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-rpart_4.1-12-1cran1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-survival_2.41-3-2xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-class_7.3-14-2xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-nnet_7.3-12-2xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-spatial_7.3-11-1xenial0_amd64.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-cran-codetools_0.2-15-1cran1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-recommended_3.4.3-1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-base_3.4.3-1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-base-dev_3.4.3-1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-base-html_3.4.3-1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Failed to fetch https://cran.rstudio.com/bin/linux/ubuntu/xenial/r-doc-html_3.4.3-1xenial0_all.deb server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
```

原先加入 repository 為 cran.rstudio.com 改成 cran.csie.ntu.edu.tw，但 cran.rstudio.com 還是被擋，可以與資訊室申請憑證。

```
# 新增 cran.rstudio.com repository 
sudo add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
# 移除 repository 
sudo add-apt-repository --remove 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
# 新增 cran.csie.ntu.edu.tw repository 
sudo add-apt-repository 'deb [arch=amd64,i386] https://cran.csie.ntu.edu.tw/bin/linux/ubuntu xenial/'
```

進入 root

```
vim /etc/apt/sources.list
```

刪除

```
deb [arch=i386,amd64] http://cran.csie.ntu.edu.tw/bin/linux/ubuntu xenial/
# deb-src [arch=i386,amd64] http://cran.csie.ntu.edu.tw/bin/linux/ubuntu xenial/
```

[憑證設定](/post/2019-05-14-linux-憑證設定/)


## 安裝套件

--- 

```
.libPaths()
```

get.

```
[1] "/home/rserver/R/x86_64-pc-linux-gnu-library/3.4"
[2] "/usr/local/lib/R/site-library"                  
[3] "/usr/lib/R/site-library"                        
[4] "/usr/lib/R/library"
```

安裝套件

```
sudo su -c "R -e \"install.packages('shiny', repos='https://cran.rstudio.com/')\""
sudo su -c "R -e \"install.packages('shiny', repos='https://mran.revolutionanalytics.com')\""
```

移除套件

```
sudo su -c "R -e \"remove.packages('curl')\""
```

# microsoft R R_LIBS_SITE 問題

--- 

microsoft-r-open-3.4.3 R_LIBS_SITE 預設會跑掉，以至於套件無法正常讀取，需編輯 `Renviron` 

```info
發現不要直接用 apt 安裝不會有這個問題，建議官方下載文件安裝
```

查看 R 環境如下

```
R_HOME                           /opt/microsoft/rclient/3.4.3/runtime/R
R_INCLUDE_DIR                    /opt/microsoft/rclient/3.4.3/runtime/R/include
R_LIBS_SITE                      /opt/microsoft/rclient/3.4.3/libraries/RServer
R_LIBS_USER                      ~/R/x86_64-pc-linux-gnu-library/3.4
```

```bash=
vim /opt/microsoft/rclient/3.4.3/runtime/R/etc/Renviron
```

Renviron

```Renviron=
R_LIBS_SITE='/opt/microsoft/rclient/3.4.3/libraries/RServer'
```

# 其他環境設定

---

## Configuring the Server

---

from: https://support.rstudio.com/hc/en-us/articles/200552316-Configuring-the-Server

### Network Port and Address
After initial installation RStudio accepts connections on port 8787. If you wish to change to another port you should create an /etc/rstudio/rserver.conf file (if one doesn't already exist) and add a www-port entry corresponding to the port you want RStudio to listen on. For example:

www-port=80
By default RStudio binds to address 0.0.0.0 (accepting connections from any remote IP). You can modify this behavior using the www-address entry. For example:

www-address=127.0.0.1
Note that after editing the /etc/rstudio/rserver.conf file you should always restart the server to apply your changes (and validate that your configuration entries were valid). You can do this by entering the following command:

```
sudo rstudio-server restart
```

### External Libraries

You can add elements to the default LD_LIBRARY_PATH for R sessions (as determined by the R ldpaths script) by adding an rsession-ld-library-path entry to the server config file. This might be useful for ensuring that packages can locate external library dependencies that aren't installed in the system standard library paths. For example:

```
rsession-ld-library-path=/opt/local/lib:/opt/local/someapp/lib
```

### Specifying R Version

By default RStudio Server runs against the version of R which is found on the system PATH (using which R). You can override which version of R is used via the rsession-which-r setting in the server config file. For example, if you have two versions of R installed on the server and want to make sure the one at /usr/local/bin/R is used by RStudio then you would use:

```
rsession-which-r=/usr/local/bin/R
```

Note again that the server must be restarted for this setting to take effect.

### Setting User Limits

There are a number of settings which place limits on which users can access RStudio and the amount of resources they can consume. This file does not exist by default so if you wish to specify any of the settings below you should create the file.

To limit the users who can login to RStudio to the members of a specific group, you use the auth-required-user-group setting. For example:

```
auth-required-user-group=rstudio_users
```

### Additional Settings

There is a separate /etc/rstudio/rsession.conf configuration file that enables you to control various aspects of R sessions (note that as with rserver.conf this file does not exist by default). These settings are especially useful if you have a large number of potential users and want to make sure that resources are balanced appropriately.

Session Timeouts
By default if a user hasn't issued a command for 2 hours RStudio will suspend that user's R session to disk so they are no longer consuming server resources (the next time the user attempts to access the server their session will be restored). You can change the timeout (including disabling it by specifying a value of 0) using the session-timeout-minutes setting. For example:

session-timeout-minutes=30
Note that a user's session will never be suspended while it is running code (only sessions which are idle will be suspended).

As of RStudio version 1.0 session timeout settings can be customized for individual users or groups. You can add session-timeout-minutes to the /etc/rstudio/profiles file:

```
[user]
session-timeout-minutes=30
 
[@powerusers]
session-timeout-minutes=0
```

Package Library Path
By default RStudio sets the R_LIBS_USER environment variable to ~/R/library. This ensures that packages installed by end users do not have R version numbers encoded in the path (which is the default behavior). This in turn enables administrators to upgrade the version of R on the server without reseting users installed packages (which would occur if the installed packages were in an R-version derived directory).

If you wish to override this behavior you can do so using the r-libs-user settings. For example:

```
r-libs-user=~/R/packages
```

CRAN Repository
Finally, you can set the default CRAN repository for the server using the r-cran-repos setting. For example:

```
r-cran-repos=https://mirrors.nics.utk.edu/cran/
```

Note again that the above settings should be specified in the `/etc/rstudio/rsession.conf` file (rather than the aforementioned rserver.conf file).


## Need a way to abort session recovery in RStudio Server


>Sometimes I'm working on a huge workspace which takes forever to save to disk and recover from disk. If I forget to cleanup, after a night the workspace is saved to disk so that the memory is freed. Whenever I continue the session, it will automatically attempt to recover from disk. When the workspace is huge and I want a new session in a hurry, I can't immediately get to a fresh new session even I run rstudio-server kill-all. That is, I don't have a decent way to abandon the previous session unless I disable session timeout (which is not ok because the server needs much memory in the morning for production), or delete files ~/.rstudio/sessions/acitve.

> I'm wondering if RStudio Server can check the size of the workspace and prompt if it's too big before restoring it so that I can choose to abandon it?

> from: https://github.com/rstudio/rstudio/issues/1633

solve:

1. 無法自動在每次登入自動清除 workspace，由於 Rstudio Server 不會自動針對每個 session 做登出的動作，session 一直都在執行，如需離開參照 session，Option -> Session -> Quit Session...

from: https://support.rstudio.com/hc/en-us/community/posts/200666347-Quit-and-logout

2. 如果讀取量過大，下次登入會需要一段時間，想拋棄這些暫存資料，可刪除 .rstudio 資料夾 (需 root 權限或本身使用者刪除)

# Java 問題

```bash=
vim /opt/microsoft/ropen/3.5.2/lib64/R/etc/Renviron
```

add JAVA_HOME in Renviron

```Renviron
JAVA_HOME='/usr/lib/jvm/java-1.8.0-openjdk-amd64/'
```
