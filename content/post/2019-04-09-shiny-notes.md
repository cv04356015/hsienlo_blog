---
draft: false
layout: blog
title: Shiny Notes
author: Hsien Ching Lo
date: 2019-04-09T03:01:29.949Z
categories:
  - Shiny
  - R
tags:
  - Shiny
  - R
Keywords:
  - Shiny
  - R
---

ref<sup>1</sup>: Shiny 常用篩選功能的範例套用網站 [Shiny Widgets Gallery](https://shiny.rstudio.com/gallery/widget-gallery.html)

# 資料蒐集

依分類列出功能型 Shiny 面板

* 地圖分析
  * [SpatialEpiApp](https://github.com/Paula-Moraga/SpatialEpiApp)

* 統計監測分析
  * [memapp](https://github.com/lozalojo/memapp)


# 建立一個 Shiny Dashboard

ref<sup>1</sup>: [Shiny Dashboard](https://rstudio.github.io/shinydashboard/index.html)

ref<sup>2</sup>: [Build your first web app dashboard using Shiny and R](https://medium.freecodecamp.org/build-your-first-web-app-dashboard-using-shiny-and-r-ec433c9f3f6c)

```
## app.R ##
library(shiny)
library(shinydashboard)
ui <- dashboardPage(
  dashboardHeader(),
  dashboardSidebar(),
  dashboardBody()
)
server <- function(input, output) {
}
shinyApp(ui, server)
```

<!--more-->

# 儀表版、量測儀(gauge)

ref<sup>1</sup>: [gauge-shiny](https://www.rdocumentation.org/packages/flexdashboard/versions/0.5.1.1/topics/gauge-shiny) 

ref<sup>2</sup>: [FrissAnalytics/shinyJsTutorials](https://github.com/FrissAnalytics/shinyJsTutorials) (運用 javascript in shiny 達到相同的效果)

# 設計按鈕讓 Shiny 頁面回到頂部

ref<sup>1</sup>: [How to add a “back to top of page” button in R Shiny?](https://stackoverflow.com/questions/44423716/how-to-add-a-back-to-top-of-page-button-in-r-shiny) 

# Shiny 彈跳視窗

ref<sup>1</sup>: [shinyalert: Easily create pretty popup messages (modals) in Shiny](https://deanattali.com/blog/shinyalert-package/)

ref<sup>2</sup>: [shinyalert-demo](https://daattali.com/shiny/shinyalert-demo/)

# Shiny 面板中套用 Ace 編輯器

ref<sup>1</sup>: [trestletech/shinyAce](https://github.com/trestletech/shinyAce)

ref<sup>2</sup>: [Simple Shiny Ace!](https://starkingdom.shinyapps.io/shinyAce-01-basic/)

ref<sup>3</sup>: [shinyAce Code Autocompletion Demo](https://trestletech.shinyapps.io/shinyAce6/)


# DataTable 不顯示其他功能(Search, footnote, ...)

use `dom = 't'`

```
library(DT)
d = cbind(
  head(iris),
  Mixed = c(1, 2, 3, '10', '1a', '1b')
)
datatable(d, options = list(dom = 't'))
```

# World cloud Click Event in shiny?

[function to give wordcloud2 click interactivity](https://github.com/Lchiffon/wordcloud2/issues/25)

```
wc2ClickedWord = function(cloudOutputId, inputId) {
  shiny::tags$script(shiny::HTML(
    sprintf("$(document).on('click', '#%s', function() {", cloudOutputId),
    'word = document.getElementById("wcSpan").innerHTML;',
    sprintf("Shiny.onInputChange('%s', word);", inputId),
    "});"
  ))
}
```

# Create a modal dialog UI


# 擷取操作設備框架大小於 shiny 之應用 

```
library(shiny)

# Define UI for application that draws a histogram
ui <- shinyUI(fluidPage(
  
  # Application title
  titlePanel("Old Faithful Geyser Data"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      tags$head(tags$script('
                            var dimension = [0, 0];
                            $(document).on("shiny:connected", function(e) {
                            dimension[0] = window.innerWidth;
                            dimension[1] = window.innerHeight;
                            Shiny.onInputChange("dimension", dimension);
                            });
                            $(window).resize(function(e) {
                            dimension[0] = window.innerWidth;
                            dimension[1] = window.innerHeight;
                            Shiny.onInputChange("dimension", dimension);
                            });
                            ')),
      sliderInput("bins",
                  "Number of bins:",
                  min = 1,
                  max = 50,
                  value = 30)
      ),
    
    # Show a plot of the generated distribution
    mainPanel(
      verbatimTextOutput("dimension_display"),
      plotOutput("distPlot")
    )
    )
))

# Define server logic required to draw a histogram
server <- shinyServer(function(input, output) {
  output$dimension_display <- renderText({
    paste(input$dimension[1], input$dimension[2], input$dimension[2]/input$dimension[1])
  })
  
  output$distPlot <- renderPlot({
    # generate bins based on input$bins from ui.R
    x    <- faithful[, 2] 
    bins <- seq(min(x), max(x), length.out = input$bins + 1)
    
    # draw the histogram with the specified number of bins
    hist(x, breaks = bins, col = 'darkgray', border = 'white')
  })
})

# Run the application 
shinyApp(ui = ui, server = server)
```

# shinyalert

這個 [Github](https://daattali.com/shiny/shinyalert-demo/) 是 shinyalert 的範例，可透過點選方式找到你要的效果。

或安裝完 shinyalert 後執行本段

```
runExample <- function() {
  appDir <- system.file("examples", "demo", package = "shinyalert")
  shiny::runApp(appDir, display.mode = "normal")
}
runExample()
```

簡單呈現

```
library(shiny)
library(shinyalert)

ui <- fluidPage(
  useShinyalert(),  # Set up shinyalert
  actionButton("preview", "Preview")
)

server <- function(input, output, session) {
  observeEvent(input$preview, {
    # Show a modal when the button is pressed
    shinyalert("Oops!", "Something went wrong.", type = "error")
  })
}

shinyApp(ui, server)
```
