---
draft: false
layout: blog
title: PostgreSQL 連接外部資料庫
author: Hsien Ching Lo
date: 2019-04-30T02:50:59.040Z
categories:
  - PostgreSQL
tags:
  - PostgreSQL
Keywords:
  - PostgreSQL
---
當我從 oracle 讀取資料時，沒辦法讀取完整的資料，每次加總後發現資料量都是少的(看似有去重複的狀況，但是不知道增加 primary key 會不會解決)。

但如果透過一個 View 做為中介就不一樣了，我們就可以正常讀取了。

原始寫法如下：

```flow
st=>start: Oracle SQL view
e=>end: PostgreSQL table
op1=>operation: dblink
st(right)->op1(right)->e
```

需要這樣寫：

```flow
st=>start: Oracle SQL view
e=>end: PostgreSQL table
op1=>operation: dblink
sub1=>subroutine: Create a View
st(right)->sub1(right)->op1(right)->e
```

<!--more-->

1.建立一個 foreign table

![](/blog/images/pasted-27.png)

2.建立一個 View table 於 postgres 資料庫下面

```sql
DROP VIEW IF EXISTS v_fact_rods_report_all CASCADE; -- 如果 View 存在就刪除
CREATE VIEW v_fact_rods_report_all AS
SELECT * FROM public.v_fact_rods_report ORDER BY ADMIT_DATE
```

![](/blog/images/pasted-28.png)

3.透過 dblink 取得其他資料庫的資料

```sql
DROP TABLE IF EXISTS rods_visit CASCADE;
CREATE TABLE rods_visit AS
SELECT t2.YEARWEEK, t1.ADMIT_DATE, t3.BRANCH, t3.COUNTY, t3.HOSPITAL, t1.HOSPITAL_ID, t1.AGE, t1.SEX, t1.E_I, t1.HERPANGINA, t1.H_F_M_DISEASE, t1.RODS_RS, t1.ESSENCE_ILI, t1.PINK_EYE, t1.RODS_GI, t1.ACUTEDIARRHEA, t1.DENGUE, t1.FEVER, t1.HTD, t1.HD, t1.CD, t1.DM, t1.HEAT_TYPE1, t1.HEAT_TYPE2, t1.HEAT_TYPE3, t1.HEAT_TYPE4, t1.HEAT_TYPE5, t1.SCARLETFEVER, t1.CHICKENPOX, t1.E9060, t1.E9061, t1.E9065, t1.V015, t1.BADGER, t1.WILDLIFE, t1.FACED_CIVET, t1.TOTAL
FROM (SELECT *
FROM dblink('dbname=postgres', 'select * from public.v_fact_rods_report_all')
AS (
	HOSPITAL_ID     text,
	ADMIT_DATE      DATE,
	AGE     integer,
	SEX     text,
	E_I     integer,
	HERPANGINA      integer,
	H_F_M_DISEASE   integer,
	RODS_RS integer,
	ESSENCE_ILI     integer,
	PINK_EYE        integer,
	RODS_GI integer,
	ACUTEDIARRHEA   integer,
	DENGUE  integer,
	FEVER   integer,
	HTD     integer,
	HD      integer,
	CD      integer,
	DM      integer,
	HEAT_TYPE1      integer,
	HEAT_TYPE2      integer,
	HEAT_TYPE3      integer,
	HEAT_TYPE4      integer,
	HEAT_TYPE5      integer,
	SCARLETFEVER    integer,
	CHICKENPOX      integer,
	E9060   integer,
	E9061   integer,
	E9065   integer,
	V015    integer,
	BADGER  integer,
	WILDLIFE        integer,
	FACED_CIVET     integer,
	TOTAL   integer
)) t1
LEFT JOIN (SELECT * FROM
   dblink('dbname=DIM_DATA', 'select date, yearweek from public.dim_weekdate') AS t1(date date, yearweek text)) t2 ON t1.ADMIT_DATE = t2.date
LEFT JOIN (SELECT * FROM
   dblink('dbname=DIM_DATA', 'select hos_id, name, new_name, branch from public.dim_hospital') AS t1(hos_id text, hospital text, county text, branch text)) t3 ON t1.HOSPITAL_ID = t3.hos_id;
```

4.驗證資料

```sql
SELECT admit_date, 
SUM(total) AS total
FROM rods_visit
-- WHERE admit_date >= '20180101'
-- WHERE admit_date < now() - interval '30 days'
GROUP BY admit_date
ORDER BY admit_date
```

