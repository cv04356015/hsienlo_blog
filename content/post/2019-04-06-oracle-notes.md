---
draft: false
layout: blog
title: Oracle Notes
author: Hsien Ching Lo
date: 2019-04-06T14:45:44.660Z
categories:
  - Oracle
tags:
  - Oracle
Keywords:
  - Oracle
---
# 環境設定

## 語法包含中文語句會造成讀到亂碼

```sql
ALTER SESSION SET NLS_LANGUAGE = 'TRADITIONAL CHINESE';
```
<!--more-->

# 資料處理
## 字串處理
### 判斷是否包含字串中的文字

Method1:

```sql
select * from students where instr(address, 'taiwan') > 0
```

Method2:

```sql
select * from students where address like '%taiwan%'
```

## 日期處理

### 日期 datetime 轉 date

ref<sup>1</sup>: [oracle SQL how to remove time from date](https://stackoverflow.com/questions/13135552/oracle-sql-how-to-remove-time-from-date)

```sql
SELECT 
     SYSTIMESTAMP "SYSTIMESTAMP",
     trunc(SYSTIMESTAMP) "SYSDATE"
     FROM DUAL;
```

```
SYSTIMESTAMP                           NOW     
-------------------------------------- ---------
08-3月 -19 12.01.01.133016000 下午 +08:00 08-3月 -19
```

## 查詢前N筆資料

```
-- Oracle
SELECT * FROM table_name WHERE ROWNUM < 10;

-- MS SQL
SELECT TOP 10 * FROM table_name ORDER BY column_name;

-- MySQL
SELECT * FROM table_name ORDER BY column_name LIMIT 10;
```


## oracle 字串編碼轉換(utf-8)

一個 table 的資料可能有多個編碼，透過 R DBI 套件讀取只能同時接受一個編碼，因此需對編碼進行前置轉換，轉換，方得正常顯示一個 table：

```
select 
    REPORT,
    REPORT_SOURCE,
    cast(NAME as nvarchar2(200)) as NAME,
    cast(IDNO as nvarchar2(40)) as IDNO,
    cast(REPORT_DISEASE as nvarchar2(80)) as REPORT_DISEASE,
    cast(DETERMINED_DISEASE as nvarchar2(80)) as DETERMINED_DISEASE,
    cast(IS_ACUTE as nchar(8)) as IS_ACUTE,
    cast(DETERMINED_STATUS as nvarchar2(60)) as DETERMINED_STATUS
from V_MORNING_MEETING_REPORT_ALL
```

## ROWNUM 關鍵字

ref:https://blog.csdn.net/null____/article/details/8264654

* 列出前 n 行

```
SELECT column_name(s) FROM table_name WHERE ROWNUM <= number 
```


* 列出最大值

-- 透過排序達到更大的活用

```
SELECT  procedure_no  FROM (SELECT  *  FROM process_card_procedure where process_card_id=421 order by cast(procedure_no as int) desc) where rownum<=1   
```

# 當 in 的參數超過 1000 個時需另找方法解決

* Union

```
select * from TestTable where ID in (1,2,3,4,...,1000)
union all
select * from TestTable where ID in (1001,1002,...)
```

* OR 

```
select * from TestTable where ID in (1,2,3,4,...,1000) or ID in (1001,1002,...,2000)
```

# 計算類型

## 函數
* count()

# 時間類型

## 函數
* trunc() 將日期時間格式轉為日期格式
```
```

## 篩選

Q1: 近 60 天的日期篩選

A1:
```
SELECT *
FROM [TABLE]
WHERE [DATE_VAR] > (CURRENT_TIMESTAMP - 60);
```

Q2: 篩選兩個時間段之間的資料

A2:
```
SELECT *
FROM [TABLE]
WHERE [DATE_VAR] between date '2017-12-01' and date '2017-12-31';
```

Q3: 篩選大於某個時間點以後的資料

A3:
```
select trunc(admit_date), sum(rods_rs) as rods_rs, sum(total) as total
from CDCDW.V_FACT_RODS_REPORT
where trunc(admit_date) >= to_date('20180701', 'yyyymmdd')
group by trunc(admit_date);
```
