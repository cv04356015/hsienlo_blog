---
draft: false
layout: blog
title: Nginx Notes
author: Hsien Ching Lo
date: 2019-04-25T08:02:04.482Z
categories:
  - Nginx
tags:
  - Nginx
Keywords:
  - Nginx
---
# 安裝 Nginx

```bash
sudo apt-get update
sudo apt-get install nginx
```

調整防火牆

```bash
# 列出防火牆資訊
sudo ufw app list
# 如果防火牆未開，需啟用防火牆
sudo ufw enable
# 允許 Nginx HTTP
sudo ufw allow 'Nginx HTTP'
# 查看防火牆狀態
sudo ufw status
```

<!--more-->

服務相關指令

```bash
sudo systemctl status nginx
sudo systemctl stop nginx
sudo systemctl start nginx
sudo systemctl restart nginx
sudo systemctl reload nginx
sudo systemctl disable nginx
sudo systemctl enable nginx
```

# 文件配置

重要文件位置

* 網頁內容 `/var/www/html`
* 主目錄 `/etc/nginx`
* 主要 nginx 配置文件(全局配置) `/etc/nginx/nginx.conf`
* 每個站點的服務氣模組的配置文件，透過 include 啟用 `/etc/nginx/sites-available`
* nginx 的服務請求 log `/var/log/nginx/access.log`
* nginx 的錯誤 log `/var/log/nginx/error.log`

網頁連結常常有對應連結錯誤的出現(ex. 圖片檔抓不到、JavaScript、CSS 讀取位置錯誤)，請新增以下三行。

```
proxy_set_header Host      $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
```

完整如下

```
server {
    listen 80;
    server_name localhost;

    charset     utf8;
    access_log    /var/log/nginx/example.access.log;

    # the default proxy_pass
    location /blog/ {
        proxy_pass http://127.0.0.1:4000/blog/;
        proxy_set_header Host      $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```

# 網頁加密

```bash
sudo sh -c "echo -n 'cv04356015:' >> /etc/nginx/.htpasswd"
sudo sh -c "openssl passwd -apr1 >> /etc/nginx/.htpasswd"
cat /etc/nginx/.htpasswd
sudo vim /etc/nginx/sites-enabled/default
```

在想要加密的地方新增

```
auth_basic "Restricted Content";
auth_basic_user_file /etc/nginx/.htpasswd;
```

ex.

```
server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /usr/share/nginx/html;
    index index.html index.htm;

    server_name localhost;

    location / {
        try_files $uri $uri/ =404;
        auth_basic "Restricted Content";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }
}
```

重啟

```bash
sudo systemctl restart nginx
```

# 關閉某個網址

關閉網頁下某個網址 `https://r.cdc.gov.tw/shiny/application/`

```
location ^~ /shiny/application/ {
    deny all;
}
```

# 同時放置兩個 server 

```
server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name hsienlo.ga;

    ssl on;
    ssl_certificate /etc/letsencrypt/live/hsienlo.ga/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/hsienlo.ga/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    # web socket
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    
    charset     utf8;
    access_log  /var/log/nginx/example.access.log;
    root /home/cv04356015/homepage;
    index index.html;

    location /rstudio/ {
        proxy_pass http://127.0.0.1:8787/;
    }

    location /shiny/ {
        proxy_pass http://127.0.0.1:3838/;
    }

}

server {
    listen 80;
    server_name blog.hsienlo.ga;

    charset     utf8;
    access_log    /var/log/nginx/example.access.log;

    # web socket
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";

    # the default proxy_pass
    location / {
        proxy_pass http://127.0.0.1:4000/;
        auth_basic "Restricted Content";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }
}

server {
    listen 80;
    server_name note.hsienlo.ga;

    charset     utf8;
    access_log    /var/log/nginx/example.access.log;

    # web socket
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";

    # the default proxy_pass
    location / {
        proxy_pass http://127.0.0.1:4001/;
    }
}
```

# 相關連結

ref<sup>1</sup>: [nginx 安裝於 ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04)

ref<sup>2</sup>: [防火牆相關指令](http://www.arthurtoday.com/2013/12/ubuntu-ufw-add-firewall-rules.html)

ref<sup>3</sup>: [How To Set Up Password Authentication with Nginx on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-nginx-on-ubuntu-14-04)
