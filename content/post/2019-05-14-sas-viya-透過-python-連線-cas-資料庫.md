---
draft: false
layout: blog
title: SAS Viya 透過 Python 連線 CAS 資料庫
author: Hsien Ching Lo
date: 2019-05-14T03:59:11.006Z
categories:
  - Python
  - SAS Viya
tags:
  - Python
  - SAS Viya
Keywords:
  - Python
  - SAS Viya
---
# Python 連線 CAS 資料庫 

* [Github 連結](https://github.com/sassoftware/python-swat) 
* [python-swat 透過 https 連接](https://github.com/sassoftware/python-swat/issues/20)

先加入環境變數

``` bash
export CAS_CLIENT_SSL_CA_LIST=/opt/sas/viya/config/etc/SASSecurityCertificateFramework/cacerts/trustedcerts.pem
```

連線程式碼如下：

``` python
import swat
conn = swat.CAS("cas-url", 5570, username = "sasdemo01", password = "demopw")
```

在 Windows 版本下，需參考新增憑證的方式。[連結](http://go.documentation.sas.com/?cdcId=pgmsascdc&cdcVersion=9.4_3.3&docsetId=secref&docsetTarget=n12036intelplatform00install.htm&locale=zh-TW)

<!--more-->
