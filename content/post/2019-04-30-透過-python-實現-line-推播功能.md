---
draft: false
layout: blog
title: 透過 Python 實現 LINE 推播功能
author: Hsien Ching Lo
date: 2019-04-30T02:34:02.433Z
categories:
  - LINE
  - Python
tags:
  - LINE
  - Python
Keywords:
  - LINE
  - Python
---
```python
import os
from linebot import LineBotApi
from linebot.models import TextSendMessage
from linebot.exceptions import LineBotApiError

line_bot_api = LineBotApi('L+bDuYSwo2Y30Dy+Xv3nbnRvoFouZfi3ss18yBTh/QXjWW0GwvjECFrl26kTN+SRZW/nsik7ZEEJX+CFq2SollO6KiqTs3K24GMi5IwBCWvloExaZMfI5olG0IYG6RqPrypvqyzXajWr9j5GQcfFNgdB04t89/1O/w1cDnyilFU=')

line_bot_api.push_message('6k43fTIHVS', TextSendMessage(text='test'))
```

<!--more-->
