---
draft: false
layout: blog
title: Gitlab 建置於 Ubuntu
author: Hsien Ching Lo
date: 2019-04-29T09:08:01.574Z
categories:
  - Gitlab
  - Ubuntu
tags:
  - Gitlab
  - Ubuntu
Keywords:
  - Gitlab
  - Ubuntu
---
# 安裝 Gitlab(docker)

* [gitlab/gitlab-ce](https://hub.docker.com/r/gitlab/gitlab-ce/)


Pull the new image:

```bash
sudo docker pull gitlab/gitlab-ce:latest
```

Create the container once again with previously specified options:

mkdir

```bash
sudo mkdir /srv/gitlab/config
sudo mkdir /srv/gitlab/logs
sudo mkdir /srv/gitlab/data
```

```bash
sudo docker run \
-p 1429:80 -d \
--name gitlab \
--volume /srv/gitlab/config:/etc/gitlab \
--volume /srv/gitlab/logs:/var/log/gitlab \
--volume /srv/gitlab/data:/var/opt/gitlab \
gitlab/gitlab-ce
```

```bash
sudo docker run --detach \
    --hostname r.cdc.gov.tw \
    --publish 1431:443 --publish 1432:80 --publish 1433:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce
```

```bash
sudo docker rm gitlab
```

list docker

```bash
sudo docker ps -a
```
