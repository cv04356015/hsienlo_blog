---
draft: false
layout: blog
title: 透過 Python 實現 LINE 通知功能
author: Hsien Ching Lo
date: 2019-04-30T02:31:58.053Z
categories:
  - LINE
  - Python
tags:
  - LINE
  - Python
Keywords:
  - LINE
  - Python
---
# 透過 Python 實現 LINE 通知功能

```python
# -*- coding: utf-8 -*-

import sys
# -----------------------------------------------------------------------------
# you can comment it, there is only one function using it (sendMsgByLineTool)
# import lineTool
# -----------------------------------------------------------------------------
import requests

# def sendMsgByLineTool(getToken, getMsg):
#    lineTool.lineNotify(getToken, getMsg)
    
def sendMsgByOriginPackage(getToken, getMsg):
    url = 'https://notify-api.line.me/api/notify'
    headers = {
        'Authorization': 'Bearer ' + getToken, 
        'Content-Type' : 'application/x-www-form-urlencoded'
    }
    payload = {'message': getMsg}
    r = requests.post(url, headers = headers, params = payload)
    return r
    
def sendMsgAndStickerByOriPkg(getToken, getMsg, getStickerPkgId, getStickerId):
    url = "https://notify-api.line.me/api/notify"
    headers = {
        "Authorization": "Bearer " + getToken
    }
    payload = {\
        "message": getMsg, \
        "stickerPackageId": getStickerPkgId, \
        'stickerId': getStickerId
    }
    r = requests.post(url, headers = headers, params = payload)
    return r
    
def sendMsg_imageFullsize(getToken, getMsg, getPicUrl):
    url = "https://notify-api.line.me/api/notify"
    headers = {
        "Content-Type" : "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + getToken
    }
    payload = {'message': getMsg,
               'imageThumbnail':getPicUrl,
               'imageFullsize': getPicUrl}
    r = requests.post(url, headers = headers, data = payload)
    return r

# necessary
token = 'urfDi7C1AArmqZPVg4IhPsL6klfYYDcHEc3fqdynOQz'

# only send the message
msg = sys.argv[1]
print(sendMsgByOriginPackage(token, msg).status_code)

# send the message and the sticker
# msg = "send the message and the sticker"
# stickerPId = 1
# stickerId = 114
# print(sendMsgAndStickerByOriPkg(token, msg, stickerPId, stickerId).status_code)

 # send the message and the image
msg = "send the message and the image"
imgPath = "https://cdcdataapi.azurewebsites.net/data_stat_img.jpeg"
sendMsg_imageFullsize(token, msg, imgPath).text

sendMsg_imageFullsize(token, msg, imgPath)
```

<!--more-->
