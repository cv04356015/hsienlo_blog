---
draft: 'false'
layout: blog
title: PostgreSQL 多執行緒設定
author: Hsien Ching Lo
date: 2019-04-06T14:32:10.989Z
categories:
  - PostgreSQL
  - Parallel
tags:
  - PostgreSQL
  - Parallel
Keywords:
  - PostgreSQL
  - Parallel
---
# 環境檢查
首先先確認當前的多執行緒環境
ref<sup>1</sup>: [postgresql 10 的并行(parallel)简介](https://blog.csdn.net/ctypyb2002/article/details/79668603)

```sql
select *
from pg_settings ps
where 1=1
and ps.name in (
'force_parallel_mode',
'max_worker_processes',
'max_parallel_workers',
'max_parallel_workers_per_gather',
'min_parallel_relation_size',-- add 9.6,remove from 10
'min_parallel_index_scan_size',
'min_parallel_table_scan_size',
'parallel_tuple_cost',
'parallel_setup_cost'
);
```
<!--more-->

# 環境修改

```bash
sudo vim /etc/postgresql/10/main/postgresql.conf
```

修改

```
max_worker_processes = 32               # (change requires restart)
max_parallel_workers_per_gather = 32    # taken from max_parallel_workers
max_parallel_workers = 32               # maximum number of max_worker_processes that
                                        # can be used in parallel queries
```

# 對單表格使用多執行緒
進入資料庫

```
\c 資料庫名稱
```
對資料表修改執行緒數目
ref<sup>1</sup>: [PgSQL · 应用案例 · PG 11 并行计算算法，参数，强制并行度设置](http://mysql.taobao.org/monthly/2018/12/09/)

```
alter table 資料表名稱 set (parallel_workers = 執行緒數目);
```

範例

```
alter table rods_visit set (parallel_workers =16);
```

# 參考文獻
ref<sup>1</sup>: [PostgreSQL 10.1 手册](http://www.postgres.cn/docs/10/index.html)
