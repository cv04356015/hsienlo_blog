---
draft: false
layout: blog
title: PostgreSQL Notes
author: Hsien Ching Lo
date: 2019-04-30T02:39:42.617Z
categories:
  - PostgreSQL
tags:
  - PostgreSQL
Keywords:
  - PostgreSQL
---
# 管理員

* 給於使用者 SELECT 表格的權限(開通某個資料庫)

```
GRANT CONNECT ON DATABASE [資料庫名稱] TO [群組/使用者名稱] WITH GRANT OPTION;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO [群組/使用者名稱];
```

# 字元轉換

[Data Type Formatting Functions](https://www.postgresql.org/docs/10/static/functions-formatting.html)

```
TO_TIMESTAMP("UPLOAD_DATE", 'YYYY-MM-DD HH24:MI:SS')
```


<!--more-->

# Foreign Data Wrappers

## 建立

建立 V_FACT_RODS_REPORT (from Oracle to PostgreSQL)

```
-- CREATE SERVER oracle_server FOREIGN DATA WRAPPER oracle_fdw OPTIONS (dbserver '//192.168.170.54:1561/DW');
-- CREATE USER MAPPING FOR CURRENT_USER SERVER oracle_server OPTIONS (user 'sas', password 'ueCr5brAD6u4rAs62t9a');

CREATE FOREIGN TABLE v_fact_rods_report (
	HOSPITAL_ID	text, 
	ADMIT_DATE	DATE, 
	AGE	integer, 
	SEX	text, 
	E_I	integer, 
	HERPANGINA	integer, 
	H_F_M_DISEASE	integer, 
	RODS_RS	integer, 
	ESSENCE_ILI	integer, 
	PINK_EYE	integer, 
	RODS_GI	integer, 
	ACUTEDIARRHEA	integer, 
	DENGUE	integer, 
	FEVER	integer, 
	HTD	integer, 
	HD	integer, 
	CD	integer, 
	DM	integer, 
	HEAT_TYPE1	integer, 
	HEAT_TYPE2	integer, 
	HEAT_TYPE3	integer, 
	HEAT_TYPE4	integer, 
	HEAT_TYPE5	integer, 
	SCARLETFEVER	integer, 
	CHICKENPOX	integer, 
	E9060	integer, 
	E9061	integer, 
	E9065	integer, 
	V015	integer, 
	BADGER	integer, 
	WILDLIFE	integer, 
	FACED_CIVET	integer, 
	TOTAL	integer)
SERVER oracle_server OPTIONS (schema 'CDCDW', table 'V_FACT_RODS_REPORT');
```

建立(build) V_DWS_RODS_REPORT (from Oracle to PostgreSQL)

```
-- CREATE SERVER oracle_server FOREIGN DATA WRAPPER oracle_fdw OPTIONS (dbserver '//192.168.170.54:1561/DW');
-- CREATE USER MAPPING FOR CURRENT_USER SERVER oracle_server OPTIONS (user 'sas', password 'ueCr5brAD6u4rAs62t9a');

CREATE FOREIGN TABLE V_DWS_RODS_REPORT (
	PID	text,
	PNAME	text,
	HOSPITAL_ID	text,
	BIRTHDAY	date,
	UPLOAD_DATE	date,
	ADMIT_DATE	date,
	AGE	integer,
	SEX	text,
	SUBJECT	text,
	ICD9_1	text,
	ICD9_2	text,
	ICD9_3	text,
	ICD9_4	text,
	E_I	integer,
	HERPANGINA	integer,
	H_F_M_DISEASE	integer,
	RODS_RS	integer,
	ESSENCE_ILI	integer,
	PINK_EYE	integer,
	RODS_GI	integer,
	ACUTEDIARRHEA	integer,
	DENGUE	integer,
	FEVER	integer,
	HTD	integer,
	HD	integer,
	CD	integer,
	DM	integer,
	HEAT_TYPE1	integer,
	HEAT_TYPE2	integer,
	HEAT_TYPE3	integer,
	HEAT_TYPE4	integer,
	HEAT_TYPE5	integer,
	SCARLETFEVER	integer,
	CHICKENPOX	integer,
	E9060	integer,
	E9061	integer,
	E9065	integer,
	V015	integer,
	BADGER	integer,
	WILDLIFE	integer,
	FACED_CIVET	integer
) 
SERVER oracle_server OPTIONS (schema 'CDCDW', table 'V_DWS_RODS_REPORT');
```

## 讀取

在 PostgreSQL 其他資料庫下讀取 postgres 資料庫的 Foreign Data

ref:[contrib-dblink-fetch](https://www.postgresql.org/docs/current/static/contrib-dblink-fetch.html)

```
SELECT * 
FROM dblink('dbname=postgres', 'select * from public.v_dws_rods_report limit 10') 
AS (
	PID	text,
	PNAME	text,
	HOSPITAL_ID	text,
	BIRTHDAY	date,
	UPLOAD_DATE	date,
	ADMIT_DATE	date,
	AGE	integer,
	SEX	text,
	SUBJECT	text,
	ICD9_1	text,
	ICD9_2	text,
	ICD9_3	text,
	ICD9_4	text,
	E_I	integer,
	HERPANGINA	integer,
	H_F_M_DISEASE	integer,
	RODS_RS	integer,
	ESSENCE_ILI	integer,
	PINK_EYE	integer,
	RODS_GI	integer,
	ACUTEDIARRHEA	integer,
	DENGUE	integer,
	FEVER	integer,
	HTD	integer,
	HD	integer,
	CD	integer,
	DM	integer,
	HEAT_TYPE1	integer,
	HEAT_TYPE2	integer,
	HEAT_TYPE3	integer,
	HEAT_TYPE4	integer,
	HEAT_TYPE5	integer,
	SCARLETFEVER	integer,
	CHICKENPOX	integer,
	E9060	integer,
	E9061	integer,
	E9065	integer,
	V015	integer,
	BADGER	integer,
	WILDLIFE	integer,
	FACED_CIVET	integer
);
```

讀取方法2 connect -> open -> fetch

```
SELECT dblink_connect('host=localhost user=postgres password=1qaz@WSX dbname=postgres');
SELECT dblink_open('foo','select PID,PNAME,HOSPITAL_ID,BIRTHDAY,UPLOAD_DATE,ADMIT_DATE,AGE,SEX,SUBJECT,ICD9_1,ICD9_2,ICD9_3,ICD9_4,E_I,HERPANGINA,H_F_M_DISEASE,RODS_RS,ESSENCE_ILI,PINK_EYE,RODS_GI,ACUTEDIARRHEA,DENGUE,FEVER,HTD,HD,CD,DM,HEAT_TYPE1,HEAT_TYPE2,HEAT_TYPE3,HEAT_TYPE4,HEAT_TYPE5,SCARLETFEVER,CHICKENPOX,E9060,E9061,E9065,V015,BADGER,WILDLIFE,FACED_CIVET from public.v_dws_rods_report limit 10000;');
SELECT * 
FROM dblink_fetch('foo',10000) 
AS (
	PID	text,
	PNAME	text,
	HOSPITAL_ID	text,
	BIRTHDAY	date,
	UPLOAD_DATE	date,
	ADMIT_DATE	date,
	AGE	integer,
	SEX	text,
	SUBJECT	text,
	ICD9_1	text,
	ICD9_2	text,
	ICD9_3	text,
	ICD9_4	text,
	E_I	integer,
	HERPANGINA	integer,
	H_F_M_DISEASE	integer,
	RODS_RS	integer,
	ESSENCE_ILI	integer,
	PINK_EYE	integer,
	RODS_GI	integer,
	ACUTEDIARRHEA	integer,
	DENGUE	integer,
	FEVER	integer,
	HTD	integer,
	HD	integer,
	CD	integer,
	DM	integer,
	HEAT_TYPE1	integer,
	HEAT_TYPE2	integer,
	HEAT_TYPE3	integer,
	HEAT_TYPE4	integer,
	HEAT_TYPE5	integer,
	SCARLETFEVER	integer,
	CHICKENPOX	integer,
	E9060	integer,
	E9061	integer,
	E9065	integer,
	V015	integer,
	BADGER	integer,
	WILDLIFE	integer,
	FACED_CIVET	integer
);
```

## 表格操作

```
# 移除資料表與其相關物件
DROP TABLE IF EXISTS [table_name] CASCADE;
```

```
# 建立 index
CREATE INDEX idx_rods_visit ON rods_visit(yearweek, admit_date, branch, county, hospital, hospital_id, age, sex);
# 更新 index
REINDEX INDEX idx_rods_visit 
```

## 資料處理

### CASE WHEN

ref:https://www.postgresql.org/docs/current/static/functions-conditional.html#FUNCTIONS-CASE

```
CASE WHEN condition THEN result
     [WHEN ...]
     [ELSE result]
END
```

ex.

```
SELECT * FROM test;

 a
---
 1
 2
 3


SELECT a,
       CASE WHEN a=1 THEN 'one'
            WHEN a=2 THEN 'two'
            ELSE 'other'
       END
    FROM test;

 a | case
---+-------
 1 | one
 2 | two
 3 | other
```

### GROUP BY

```sql
CREATE TEMP TABLE SUMMARISE AS
SELECT HOSPITAL_ID,ADMIT_DATE,AGE,SEX,SUM(E_I) AS E_I,SUM(HERPANGINA) AS HERPANGINA,SUM(H_F_M_DISEASE) AS H_F_M_DISEASE,SUM(RODS_RS) AS RODS_RS,SUM(ESSENCE_ILI) AS ESSENCE_ILI,SUM(PINK_EYE) AS PINK_EYE,SUM(RODS_GI) AS RODS_GI,
				   SUM(ACUTEDIARRHEA) AS ACUTEDIARRHEA,SUM(DENGUE) AS DENGUE,SUM(FEVER) AS FEVER,SUM(HTD) AS HTD,SUM(HD) AS HD,SUM(CD) AS CD,SUM(DM) AS DM,SUM(HEAT_TYPE1) AS HEAT_TYPE1,SUM(HEAT_TYPE2) AS HEAT_TYPE2,
				   SUM(HEAT_TYPE3) AS HEAT_TYPE3,SUM(HEAT_TYPE4) AS HEAT_TYPE4,SUM(HEAT_TYPE5) AS HEAT_TYPE5,SUM(SCARLETFEVER) AS SCARLETFEVER,SUM(CHICKENPOX) AS CHICKENPOX,SUM(E9060) AS E9060,SUM(E9061) AS E9061,SUM(E9065) AS E9065,SUM(V015) AS V015,
				   SUM(BADGER) AS BADGER,SUM(WILDLIFE) AS WILDLIFE,SUM(FACED_CIVET) AS FACED_CIVET,COUNT(HOSPITAL_ID) AS TOTAL 
FROM public.dws_rods_report
GROUP BY HOSPITAL_ID,ADMIT_DATE,AGE,SEX
ORDER BY ADMIT_DATE;
```

#### GROUP BY 取得最大値

```
SELECT max(date) as date, yearweek FROM public.dim_weekdate 
where 
yearweek = '201001' or
yearweek = '201101' or
yearweek = '201201' or
yearweek = '201301' or
yearweek = '201401' or
yearweek = '201501' or
yearweek = '201601' or
yearweek = '201701' or
yearweek = '201801'
GROUP BY yearweek;
```

## 查詢資料表欄位資訊

* 列出所有欄位

```sql
SELECT *
FROM information_schema.columns
WHERE table_schema = 'your_schema'
  AND table_name   = 'your_table'
```

ex.

```sql
SELECT *
FROM information_schema.columns
WHERE table_schema = 'public'
  AND table_name   = 'rods_visit'
```

# Bash 指令

```
#!/bin/bash

PGPASSWORD=1qaz@WSX psql \
    -X \
    -U postgres \
    -h localhost \
    -f /home/rserver/task/RODS_VISIT/RODS_VISIT.sql \
    --echo-all \
    --set AUTOCOMMIT=off \
    --set ON_ERROR_STOP=on \
    --set TSUFF=$TSUFF \
    --set QTSTUFF=\'public\' \
    -d RODS_DATA

psql_exit_status = $?

if [ $psql_exit_status != 0 ]; then
    echo "psql failed while trying to run this sql script" 1>&2
    exit $psql_exit_status
fi

echo "sql script successful"
exit 0
```

# 取得表格大小

General Table Size Information

```sql
SELECT *, pg_size_pretty(total_bytes) AS total
    , pg_size_pretty(index_bytes) AS INDEX
    , pg_size_pretty(toast_bytes) AS toast
    , pg_size_pretty(table_bytes) AS TABLE
  FROM (
  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes FROM (
      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
              , c.reltuples AS row_estimate
              , pg_total_relation_size(c.oid) AS total_bytes
              , pg_indexes_size(c.oid) AS index_bytes
              , pg_total_relation_size(reltoastrelid) AS toast_bytes
          FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE relkind = 'r'
  ) a
) a;
```

# 表格指令

* [移除 View](https://www.postgresql.org/docs/10/static/sql-dropview.html)

```sql
DROP VIEW [ IF EXISTS ] name [, ...] [ CASCADE | RESTRICT ]
```

# WEHRE statement

## 篩選近 10 天資料

current_date 為當前日期

其他日期函數[參考](https://www.postgresql.org/docs/10/static/functions-datetime.html)

```sql
SELECT Table.date 
FROM Table 
WHERE date > current_date - interval '10' day;
```

## 篩選某個日期的資料

```sql
SELECT *
FROM table
WHERE dt::date = '2011-01-01'
```

or

```sql
SELECT *
FROM table
WHERE CAST(dt AS DATE) = '2011-01-01'
```

# 產出 csv

產出 csv 檔案

```
Copy(

    ... SQL 程式碼 ...

) To '/檔案位置/檔名.csv' With CSV DELIMITER ',';
```

產出具有表頭的 csv 檔案 `HEADER`

```
Copy(

    ... SQL 程式碼 ...

) To '/檔案位置/檔名.csv' With CSV DELIMITER ',' HEADER;
```


# 相關參考

* [SQL 語法](https://www.1keydata.com/tw/sql/sql-syntax.html)
