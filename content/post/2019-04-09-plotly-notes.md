---
draft: false
layout: blog
title: Plotly Notes
author: Hsien Ching Lo
date: 2019-04-09T01:37:36.563Z
categories:
  - Plotly
  - R
  - Python
tags:
  - Plotly
  - R
  - Python
Keywords:
  - Plotly
  - R
  - Python
---


## 自訂座標軸

ref<sup>1</sup>: [Enumerated ticks with tickvals and ticktext](https://plot.ly/pandas/axes/#enumerated-ticks-with-tickvals-and-ticktext)

可透過 tickvals、ticktext 參數自訂座標軸

![](/images/uploads/2019-04-09-09-49-25-image-2.png)

這邊範例並無使用到 ticktext 

將 tickvals 的變數設計成「不想被顯示的都以 NA 表示」

<!--more-->

```
> rods_qcdata_admit$label_all_new
   [1] "2018-01-01" NA           NA           NA           NA           NA           "2018-01-07" NA           NA           NA          
  [11] NA           NA           NA           "2018-01-14" NA           NA           NA           NA           NA           NA          
  [21] "2018-01-21" NA           NA           NA           NA           NA           NA           "2018-01-28" NA           NA          
  [31] NA           NA           NA           NA           "2018-02-04" NA           NA           NA           NA           NA          
  [41] NA           "2018-02-11" NA           NA           NA           NA           NA           NA           "2018-02-18" NA          
  [51] NA           NA           NA           NA           NA           "2018-02-25" NA           NA           NA           NA          
  [61] NA           NA           "2018-03-04" NA           NA           NA           NA           NA           NA           "2018-03-11"
  [71] NA           NA           NA           NA           NA           NA           "2018-03-18" NA           NA           NA          
      
```

以年週合併來處理資料只顯示每周的第一天程式碼如下：

ref<sup>1</sup>: [R语言 生成顺序标号，累加，滞后函数](https://blog.csdn.net/t15600624671/article/details/77573566)

```R
plot_data <- transform(plot_data, index = as.integer(yearweek %>% as.factor()))
index_sum <- function(x){return(c(1:length(x)))}
plot_data <- transform(plot_data, index_sum=unlist(tapply(index, yearweek, index_sum))) %>% 
  mutate(label_all_new = ifelse(index_sum == 1, label_all, NA) %>% as.Date(origin = "1970-01-01"))
```

繪圖程式碼如下：

```R
plot_ly(plot_data) %>%
      add_trace(x = ~label_all, y = ~center, type= "scatter", mode= "lines", name= "CL", line= list(color= "#ff0021"), hoverinfo = "none") %>%
      add_trace(x = ~label_all, y = ~wv_ucl, type= "scatter", mode= "lines", name= "UCL", line= list(color= "#ff0021", dash = 'dash'), hoverinfo = "none") %>%
      add_trace(x = ~label_all, y = ~wv_lcl, type= "scatter", mode= "lines", name= "LCL", line= list(color= "#ff0021", dash = 'dash'), hoverinfo = "none", text = ~paste(year(label_all), "年", month(label_all), "月", day(label_all), "日", data_all, "人次就診")) %>%
      add_trace(x = ~label_all, y = ~data_all, type= "scatter", mode= "lines+markers", name= "上傳人次", line= list(color= "#4169E1"), hoverinfo = "text", text = ~paste(year(label_all), "年", month(label_all), "月", day(label_all), "日", data_all, "人次就診")) %>%
      add_trace(x = ~label_all, y = ~check_wv_lcl, type = 'scatter', mode= "markers", name= "異常點", marker = list(size = 10, color = 'rgba(255, 182, 193, .9)', line = list(color = 'rgba(152, 0, 0, .8)', width = 2)), hoverinfo = "text", text = ~paste(year(label_all), "年", month(label_all), "月", day(label_all), "日", data_all, "人次就診")) %>%
      layout(title = '<b>上傳情形-個別值管制圖<b>',
             margin = list(l = 40, r = 10, b = 80, t = 40, pad = 4),
             xaxis = list(title="", size=10, tickangle = 45, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=TRUE, showspikes = TRUE, tickvals = ~label_all_new, tickformat = "%Y-%m-%d"),
             yaxis = list(title = "", side="left", size=18, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=TRUE, showspikes = TRUE),
             shapes = list(list(type = "rect",
                                fillcolor = "#cccccc", line = list(color = "#cccccc"), opacity = 0.2,
                                x0 = max_labels, x1 = today("ROC"), xref = "x",
                                y0 = max_data_all, y1 = min_data_all, yref = "y")),
             annotations = list(list(x = 0.05, y = 1, showarrow = FALSE, text = "Calibration data", xref = "paper", yref = "paper"),
                                list(x = 0.95, y = 1, showarrow = FALSE, text = "New data", xref = "paper", yref = "paper"))) %>%
      config(displaylogo = FALSE,
             collaborate = FALSE,
             modeBarButtonsToRemove = list(
               'sendDataToCloud',
               'toImage',
               'autoScale2d',
               'resetScale2d',
               'hoverClosestCartesian',
               'hoverCompareCartesian',
               'collaborate'
             ))
```

## 設計散布圖的樣式

ref<sup>1</sup>: [Styling Markers in Python](https://plot.ly/python/marker-style/)

![](/images/uploads/2019-04-09-10-04-49-image-4.png)

```R
plot_ly(plot_data) %>%
      add_trace(x = ~label_all, y = ~center, type= "scatter", mode= "lines", name= "CL", line= list(color= "#ff0021"), hoverinfo = "none") %>%
      add_trace(x = ~label_all, y = ~wv_ucl, type= "scatter", mode= "lines", name= "UCL", line= list(color= "#ff0021", dash = 'dash'), hoverinfo = "none") %>%
      add_trace(x = ~label_all, y = ~wv_lcl, type= "scatter", mode= "lines", name= "LCL", line= list(color= "#ff0021", dash = 'dash'), hoverinfo = "none", text = ~paste(year(label_all), "年", month(label_all), "月", day(label_all), "日", data_all, "人次就診")) %>%
      add_trace(x = ~label_all, y = ~data_all, type= "scatter", mode= "lines+markers", name= "上傳人次", line= list(color= "#4169E1"), hoverinfo = "text", text = ~paste(year(label_all), "年", month(label_all), "月", day(label_all), "日", data_all, "人次就診")) %>%
      add_trace(x = ~label_all, y = ~check_wv_lcl, type = 'scatter', mode= "markers", name= "異常點", marker = list(size = 10, color = 'rgba(255, 182, 193, .9)', line = list(color = 'rgba(152, 0, 0, .8)', width = 2)), hoverinfo = "text", text = ~paste(year(label_all), "年", month(label_all), "月", day(label_all), "日", data_all, "人次就診")) %>%
      layout(title = '<b>上傳情形-個別值管制圖<b>',
             margin = list(l = 40, r = 10, b = 80, t = 40, pad = 4),
             xaxis = list(title="", size=10, tickangle = 45, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=TRUE, showspikes = TRUE, tickvals = ~label_all_new, tickformat = "%Y-%m-%d"),
             yaxis = list(title = "", side="left", size=18, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=TRUE, showspikes = TRUE),
             shapes = list(list(type = "rect",
                                fillcolor = "#cccccc", line = list(color = "#cccccc"), opacity = 0.2,
                                x0 = max_labels, x1 = today("ROC"), xref = "x",
                                y0 = max_data_all, y1 = min_data_all, yref = "y")),
             annotations = list(list(x = 0.05, y = 1, showarrow = FALSE, text = "Calibration data", xref = "paper", yref = "paper"),
                                list(x = 0.95, y = 1, showarrow = FALSE, text = "New data", xref = "paper", yref = "paper"))) %>%
      config(displaylogo = FALSE,
             collaborate = FALSE,
             modeBarButtonsToRemove = list(
               'sendDataToCloud',
               'toImage',
               'autoScale2d',
               'resetScale2d',
               'hoverClosestCartesian',
               'hoverCompareCartesian',
               'collaborate'
             ))
```

## Y軸數值超過1000的顯示轉換

ref<sup>1</sup>: [Set major tick labels to be displayed as scientific notation in a Plotly plot in R](https://stackoverflow.com/questions/49626679/set-major-tick-labels-to-be-displayed-as-scientific-notation-in-a-plotly-plot-in)

在 `yaxis` 增加參數 `exponentformat = 'none'`

```r
plot_ly(with_nhi_data) %>%
      add_trace(x = ~admit_date, y = ~rods_total, type= "scatter", mode= "lines", name= "RODS急診人次", line= list(color= "#4169E1"), hoverinfo = "text", text = ~paste(year(admit_date), "年", month(admit_date), "月", day(admit_date), "日", rods_total, "人次就診")) %>%
      add_trace(x = ~admit_date, y = ~nhi_total, type= "scatter", mode= "lines", name= "NHI急診人次", line= list(color= "#69e141"), hoverinfo = "text", text = ~paste(year(admit_date), "年", month(admit_date), "月", day(admit_date), "日", nhi_total, "人次就診")) %>%
      add_trace(x = ~admit_date, y = ~pct_total, type= "scatter", mode= "lines", name= "RODS急診百分率", line= list(color= "#e14169"), hoverinfo = "text", text = ~paste(year(admit_date), "年", month(admit_date), "月", day(admit_date), "日", pct_total, "%"), yaxis = "y2") %>%
      layout(title = NULL,
             # height = 780, 
             margin = list(l = 80, r = 20, b = 80, t = 40, pad = 4),
             xaxis = list(title="", size=10, tickangle = 45, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=FALSE, showspikes = TRUE, tickvals = ~admit_date_new, tickformat = "%Y-%m-%d"),
             yaxis = list(exponentformat = 'none', title = "", side="left", size=18, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=FALSE, showspikes = TRUE),
             yaxis2 = list(overlaying = "y", title = "", side="right", size=18)) %>%
      config(displaylogo = FALSE,
             collaborate = FALSE,
             modeBarButtonsToRemove = list(
               'sendDataToCloud',
               'toImage',
               'autoScale2d',
               'resetScale2d',
               'hoverClosestCartesian',
               'hoverCompareCartesian',
               'collaborate'
             ))
```

![upload successful](/images/uploads/2019-03-20-11-43-10-rshiny.png)

## 設定兩個Y軸

ref<sup>1</sup>: [Multiple y-axes chart with Plotly in R](https://stackoverflow.com/questions/41249315/multiple-y-axes-chart-with-plotly-in-r)

Step1: 在 `add_trace` 增加參數 `yaxis = "y2"`

Step2: 在 `layout` 增加參數 `yaxis2 = list(overlaying = "y", title = "", side="right", size=18)`

```r
plot_ly(with_nhi_data) %>%
      add_trace(x = ~admit_date, y = ~rods_total, type= "scatter", mode= "lines", name= "RODS急診人次", line= list(color= "#4169E1"), hoverinfo = "text", text = ~paste(year(admit_date), "年", month(admit_date), "月", day(admit_date), "日", rods_total, "人次就診")) %>%
      add_trace(x = ~admit_date, y = ~nhi_total, type= "scatter", mode= "lines", name= "NHI急診人次", line= list(color= "#69e141"), hoverinfo = "text", text = ~paste(year(admit_date), "年", month(admit_date), "月", day(admit_date), "日", nhi_total, "人次就診")) %>%
      add_trace(x = ~admit_date, y = ~pct_total, type= "scatter", mode= "lines", name= "RODS急診百分率", line= list(color= "#e14169"), hoverinfo = "text", text = ~paste(year(admit_date), "年", month(admit_date), "月", day(admit_date), "日", pct_total, "%"), yaxis = "y2") %>%
      layout(title = NULL,
             # height = 780, 
             margin = list(l = 80, r = 20, b = 80, t = 40, pad = 4),
             xaxis = list(title="", size=10, tickangle = 45, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=FALSE, showspikes = TRUE, tickvals = ~admit_date_new, tickformat = "%Y-%m-%d"),
             yaxis = list(exponentformat = 'none', title = "", side="left", size=18, showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange=FALSE, showspikes = TRUE),
             yaxis2 = list(overlaying = "y", title = "", side="right", size=18)) %>%
      config(displaylogo = FALSE,
             collaborate = FALSE,
             modeBarButtonsToRemove = list(
               'sendDataToCloud',
               'toImage',
               'autoScale2d',
               'resetScale2d',
               'hoverClosestCartesian',
               'hoverCompareCartesian',
               'collaborate'
             ))
```

![upload successful](/images/uploads/2019-03-20-12-03-08-rshiny.png)

## 堆疊長條圖(stack bar plot)

```R
library(plotly)
y1 <- runif(10, 5, 10)
y2 <- runif(10, 5, 10)
x <- 1:10
plot_ly(x = x, y = y1, type = 'bar', name = 'QoL', marker = list(color = 'red')) %>% 
  add_trace(y = y2, name = 'not', marker = list(color = 'blue')) %>% 
  layout(barmode = 'stack'
    , yaxis = list(title = 'Percentage (%)')
    , xaxis = list(title = 'Sub-Segment', showticklabels = FALSE)
  )
```

## 合併多個圖形

ref<sup>1</sup>: [Multiple Subplots with Titles](https://plot.ly/~PlotBot/922/multiple-subplots-with-titles/#code)

![](/images/uploads/2019-04-09-10-22-33-image-5.png)

## 移除 Plotly 預設的功能表

ref<sup>1</sup>: [How to custom or display modebar in plotly?](https://stackoverflow.com/questions/37437808/how-to-custom-or-display-modebar-in-plotly)
