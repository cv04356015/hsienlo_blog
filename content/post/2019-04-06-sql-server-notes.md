---
draft: false
layout: blog
title: SQL Server Notes
author: Hsien Ching Lo
date: 2019-04-06T14:41:59.003Z
categories:
  - SQL Server
tags:
  - SQL Server
Keywords:
  - SQL Server
---
# 日期處理

## 將格式 datetime 轉 date

ref<sup>1</sup>: [How to return only the Date from a SQL Server DateTime datatype](https://stackoverflow.com/questions/113045/how-to-return-only-the-date-from-a-sql-server-datetime-datatype)

```sql
SELECT CONVERT(date, getdate())
```

<!--more-->
