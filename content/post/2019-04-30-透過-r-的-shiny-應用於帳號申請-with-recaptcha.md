---
draft: false
layout: blog
title: 透過 R 的 Shiny 應用於帳號申請(with reCAPTCHA)
author: Hsien Ching Lo
date: 2019-04-30T02:52:26.317Z
categories:
  - R
  - Shiny
  - reCAPTCHA
tags:
  - R
  - Shiny
  - reCAPTCHA
Keywords:
  - R
  - Shiny
  - reCAPTCHA
---
reCAPTCHA 是 Google 的一個 js api，可用於避免機器人重複對你的網站執行動作。

目前有發現 shinyCAPTCHA 的套件可以使用，發現他的時候我大概已經進行到一半了，而且剛好我的 shiny 首頁是用 html 刻的，還需要另作修改，只好找時間再測試這個套件。[Github 頁面](https://github.com/CannaData/shinyCAPTCHA)。

網頁的格式是引用 rstudio server 的網頁

![](/images/uploads/2019-04-30-10-43-16-image-5.png)

* 修改 shiny 的部分是 app.R
* 修改網頁部分是 index.html (我的 app.R 的 ui 是直接匯入網頁)
* 新增一個 outer.js (可隨意取，由 index 引入外部 js 可避免每次修改 index.html 都要重啟，方便作業而已)

<!--more-->

注意：

* 如修改 index.html 皆需要重啟 shiny server，否則可能要等待一段時間，如果沒有管理員權限，可能要謹慎修改。


```
systemctl restart shiny-server.service
```

如果要新增 reCAPTCHA 要在 index.html 的 `<head>` 引入 google 的 reCAPTCHA api。

```
<!-- recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>
```

在要新增 reCAPTCHA 圖示的位置放置這段，網頁中就會隨即出現 

```
<div class="g-recaptcha" data-sitekey="6LdlslMUAAAAAKofAF28Y23QZLTUVjyxqZGWKYoN"></div>
```

工作流程如下

![](/images/uploads/2019-04-30-10-43-16-image-7.png)

因此要新增一個隱藏層

```
<div style="display: none;">
	<button id="submit" type="button" name="submit" class="fancy action-button shiny-bound-input">
     <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
        <tr>
        <td class="left"></td>
        <td class="inner" valign="middle" id="auto-sign-up"></td>
        <td class="right"></td>
        </tr>
      </tbody>
    </table>
</div>
```

點擊完 sign up 之後開始判斷他是否有通過 reCAPTCHA 的動作，如果有才進行下一步的 submit 驅動 shiny 並進行驗證

`id = check` 如下

```
<button id="check" type="button" name="check" class="fancy">
<table cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
		  <td class="left"></td>
		  <td class="inner" valign="middle">Sign Up</td>
		  <td class="right"></td>
		</tr>
	</tbody>
</table>
</button> 
```

嵌入 outer.js

```
<script src="outer.js"></script>
```

outer.js 內容

```
$(function() {
	$('#check').on('click', function(e) {
		var response = grecaptcha.getResponse();
		if(response.length == 0) {
			alert("請通過 Recaptcha 驗證");
		} else {
			//document.getElementById("caption").value = grecaptcha.getResponse();
			//$('#caption').val(grecaptcha.getResponse());
			Shiny.onInputChange("caption", grecaptcha.getResponse());
			
			//$('#submit').click();
			setInterval(function() {
				$('#auto-sign-up').click();
			}, 1500);
		}
	});
});
```

修改 app.R，在 shiny 中新增查看是否通過 google 驗證

```
pass = POST(url = 'https://www.google.com/recaptcha/api/siteverify',
	  content_type("application/x-www-form-urlencoded"),
	  body = list(secret = "6LdlslMUAAAAABP1J6IdzOKflQCUEfX7611hqZpP", response = input$caption, remoteip = "192.168.171.247"),
	  encode = "form"
	) %>% 
	content() %>%
	.$success
```

 if pass == true 表示 reCAPTCHA 的驗證是成功的，反之亦然。

# 參考來源

* [js 應用於 shiny](https://shiny.rstudio.com/articles/js-send-message.html)
  		- [範例1](https://js-tutorial.shinyapps.io/messageApp2/)
* [R httr 的應用](https://cran.r-project.org/web/packages/httr/vignettes/quickstart.html)
* [reCAPTCHA v2 文件](https://developers.google.com/recaptcha/docs/display)
