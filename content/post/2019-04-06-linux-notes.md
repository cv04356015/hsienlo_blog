---
draft: false
layout: blog
title: Linux Notes
author: Hsien Ching Lo
date: 2019-04-06T14:47:20.607Z
categories:
  - Linux
tags:
  - Linux
Keywords:
  - Linux
---
# 常用操作

當你在備份檔案想知道備份進度時，可透過 `watch` 來監控

```bash
cp ubuntu-15.10-desktop-amd64.iso /home/howtoing/ &
watch -n 0.1 du -s /home/howtoing/ubuntu-15.10-desktop-amd64.iso 
```

ref<sup>1</sup>: [如何运行或重复Linux命令永远每X秒](https://www.howtoing.com/run-repeat-linux-command-every-x-seconds)

```bash
find [要搜尋的資料夾] -name [檔案名稱]
```

file name: 可透過 * 表示所有的檔案

## 搜尋資料夾中文件出現指定文字的文件

```bash
grep -Rs [指定文字] [要搜尋的資料夾]
```
<!--more-->

# 帳號管理

## 列出使用者清單

```bash
cut -d: -f1 /etc/passwd
```

## usermod-帳號資訊修改

### 群組修改

ref<sup>1</sup>: [usermod-鳥哥](http://linux.vbird.org/linux_basic/0410accountmanager.php#usermod)

新增副群組

```bash
sudo usermod -G [群組名稱] [使用者名稱]
```

範例

```bash
sudo usermod -G rserver,root,adm,jenkins,acerincdc eva83531
```


# 資料備份

ref<sup>1</sup>: [linux下rsync和tar增量备份梳理](https://www.cnblogs.com/kevingrace/p/6601088.html)

## rsync

```bash
# rsync -az --delete 來源位置 輸出位置
rsync -az --delete /home /media/sf_Eic03-2/RBackup/User/
```

## tar.gz

### 備份目前目錄

ref<sup>1</sup>: [17.8.2. 目錄備份](https://www.freebsd.org/doc/zh_TW/books/handbook/backup-basics.html)

```bash
tar czvf /tmp/mybackup.tgz . 
```

### 還原目前目錄

```bash
tar xzvf /tmp/mybackup.tgz
```



# 其他

## 查看當前資料夾的所有資料夾目錄大小

```bash
sudo du -h --max-depth=1
```

## 查看當前目錄每秒的資料夾大小

ref<sup>1</sup>: [如何运行或重复Linux命令永远每X秒](https://www.howtoing.com/run-repeat-linux-command-every-x-seconds)

當你在備份東西，且想知道執行進度的時候很好用。

```bash
sudo watch -n 1 du -sh
```

# 誤刪檔案之復原

ref<sup>1</sup>: [解救不小心 rm 的檔案](https://sam.liho.tw/2015/11/26/linux-%E8%A7%A3%E6%95%91%E4%B8%8D%E5%B0%8F%E5%BF%83-rm-%E7%9A%84%E6%AA%94%E6%A1%88/)
