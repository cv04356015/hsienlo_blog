---
draft: false
layout: blog
title: Docker Notes
author: Hsien Ching Lo
date: 2019-04-07T06:39:56.875Z
categories:
  - Docker
tags:
  - Docker
Keywords:
  - Docker
---
# 常用指令

| 指令                                                                       | 說明                                                        |
| ------------------------------------------------------------------------ | --------------------------------------------------------- |
| docker ps                                                                | 列出所有 Docker 容器                                            |
| docker stop DOCKER_ID                                                    | 停止 Docker 容器                                              |
| docker kill DOCKER_ID                                                    | 強制停止 Docker 容器                                            |
| docker restart DOCKER_ID                                                 | 重新啟動 Docker 容器                                            |
| docker pause DOCKER_ID                                                   | 暫停 Docker 容器                                              |
| docker unpause DOCKER_ID                                                 | 恢復 Docker 容器                                              |
| docker cp /path/to/file1 DOCKER_ID:/path/to/file2                        | 複製檔案，把實體機器的 /path/to/file1 複製到 Docker 容器中的 /path/to/file2 |
| docker cp /path/to/folder DOCKER_ID:/another/path/                       | 複製目錄                                                      |
| docker cp DOCKER_ID:/path/to/file1 /path/to/file2                        | 反向複製檔案                                                    |
| docker run -it -v /home/seal/data:/data tensorflow/tensorflow bash       | 掛載目錄                                                      |
| docker run -it --cpus=1.5 agileek/cpuset-test                            | 限制 CPU 使用量(預設並不會限制容器的 CPU 使用量)                            |
| docker run -it --memory=300m --memory-swap=1g tensorflow/tensorflow bash | 限制記憶體使用量(預設並不會限制容器的 RAM 使用量)                              |
| docker run -it -p 80:8888 tensorflow/tensorflow                          | 設定網路連接埠對應                                                 |
| docker stats                                                             | 查看 CPU、記憶體與網路用量                                           |
| docker top DOCKER_ID                                                     | 查看 Docker 容器內部的行程                                         |

<!--more-->

# 容器的存儲機制

如果換了一個 container，docker 的檔案可能會消失(docker stop, rm 後再開啟，會是不同的 ，但如果有透過 volumns，以正常的文件或者目錄的形式存在於主機上，則下次可以直接讀入。

> Docker鏡像是由多個文件系統（只讀層）疊加而成。當我們啟動一個容器的時候，Docker會加載只讀鏡像層並在頂部添加一個讀寫層。如果運行中的容器修改了現有的一個已經存在的文件，那該文件將會從讀寫層下面的只讀層復制到讀寫層，該文件的只讀版本仍然存在，只是已經被讀寫層中該文件的副本所隱藏。當刪除Docker容器，並通過該鏡像重新啟動時，之前的更改將會丟失。在Docker中，只讀層及在頂部的讀寫層的組合被稱為Union File System（聯合文件系統）。
為了能夠保存（持久化）數據以及共享容器間的數據，Docker提出了Volume的概念。簡單來說，Volume就是目錄或者文件，它可以繞過默認的聯合文件系統，而以正常的文件或者目錄的形式存在於宿主機上。



