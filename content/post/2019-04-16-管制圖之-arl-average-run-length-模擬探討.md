---
draft: false
layout: blog
title: 管制圖之 ARL(Average Run Length) 模擬探討
author: Hsien Ching Lo
date: 2019-04-16T07:48:26.752Z
categories:
  - R
  - Quality Control
tags:
  - R
  - Average Run Length
  - ARL
  - Quality Control
Keywords:
  - R
  - Average Run Length
  - ARL
  - Quality Control
---
# 平均連串長度(Average Run length, ARL)

ref<sup>1</sup>: [鄭春生 - 品質管制補充教材](http://qc.iem.yzu.edu.tw/file/%E7%AE%A1%E5%88%B6%E5%9C%96%E5%8E%9F%E7%90%86.pdf)

ref<sup>2</sup>: [使用ARL（平均運行長度）來確定控製圖的性能](https://community.jmp.com/t5/JMPer-Cable/Using-ARL-Average-Run-Length-to-determine-the-performance-of-a/ba-p/61895)

連串長度(Run length)：參數(例如：平均值、標準差)改變後，管制圖偵測到異常所需之樣本組數。

平均連串長度(Average Run length, ARL)

型I誤差 ($\alpha$)：管制內 ARL，製程為正常時的ARL。(望大)

管制內 ARL $= \frac{1}{\alpha} $

型II誤差 ($\beta$)：管制外 ARL，製程為異常時的ARL。(望小)

管制外 ARL $= \frac{1}{(1 - \beta)} $

$$\beta = P\\{落在界限內|平均數已偏移\\} = P\\{LCL \leq \bar{x} \leq UCL|\mu_1 = \mu_0 + k\sigma\\}$$


<!--more-->

# 模擬資料

## 以套件方式取得 ARL 理論值

ref<sup>1</sup>: [xshewhartrunsrules.arl: Compute ARLs of Shewhart control charts with and without runs...](https://rdrr.io/cran/spc/man/xshewhartrunsrules.arl.html)

```{r}
library(spc)
## Champ/Woodall (1987)
## Table 1
mus <- (0:15)/5
Mxshewhartrunsrules.arl <- Vectorize(xshewhartrunsrules.arl, "mu")
# standard (1 of 1 beyond 3 sigma) Shewhart chart without runs rules
C1 <- round(Mxshewhartrunsrules.arl(mus, type="1"), digits=2)
# standard + runs rule: 2 of 3 beyond 2 sigma on the same side
C12 <- round(Mxshewhartrunsrules.arl(mus, type="12"), digits=2)
# standard + runs rule: 4 of 5 beyond 1 sigma on the same side
C13 <- round(Mxshewhartrunsrules.arl(mus, type="13"), digits=2)
# standard + runs rule: 8 of 8 on the same side of the center line
C14 <- round(Mxshewhartrunsrules.arl(mus, type="14"), digits=2)
data.frame(mus, C1, C12, C13, C14)
```

```
## original results are
#  mus     C1    C12    C13    C14                                    
#  0.0 370.40 225.44 166.05 152.73                                    
#  0.2 308.43 177.56 120.70 110.52                                    
#  0.4 200.08 104.46  63.88  59.76                                    
#  0.6 119.67  57.92  33.99  33.64                                    
#  0.8  71.55  33.12  19.78  21.07                                    
#  1.0  43.89  20.01  12.66  14.58                                    
#  1.2  27.82  12.81   8.84  10.90                                    
#  1.4  18.25   8.69   6.62   8.60                                    
#  1.6  12.38   6.21   5.24   7.03                                    
#  1.8   8.69   4.66   4.33   5.85                                    
#  2.0   6.30   3.65   3.68   4.89                                    
#  2.2   4.72   2.96   3.18   4.08                                    
#  2.4   3.65   2.48   2.78   3.38                                    
#  2.6   2.90   2.13   2.43   2.81                                    
#  2.8   2.38   1.87   2.14   2.35                                    
#  3.0   2.00   1.68   1.89   1.99
```

ref<sup>1</sup>: [模擬方法在統計品管研究的應用](http://140.125.88.13/home/lab/qre/source/QC/files/%E6%A8%A1%E6%93%AC%E6%96%B9%E6%B3%95%E5%9C%A8%E7%B5%B1%E8%A8%88%E5%93%81%E7%AE%A1%E7%A0%94%E7%A9%B6%E7%9A%84%E6%87%89%E7%94%A8.pdf)

ref<sup>2</sup>: [使用R做ARL的蒙地卡羅模擬](http://xuyt.blogspot.com/2013/03/rarl.html)

## 以多個模擬資料計算 ARL

* 以個別值管制圖(Individual Control Chart)為例

方法一

* alpha以超出管制圖之異常個數計算
* 以中位數較貼近理論值

```R
library(doParallel)
library(tidyverse)

getRL <- function(data, mu = NULL, sigma = NULL, k = 3, delta = 0){
  m <- length(data)
  
  if(!is.null(mu) & !is.null(sigma)) {
    ucl <- mu + k * sigma
    lcl <- mu - k * sigma
  } else if (is.null(mu) & !is.null(sigma)){
    stop("Missing mu")
  } else if (!is.null(mu) & is.null(sigma)){
    stop("Missing sigma")
  } else {
    mu_hat <- data %>% mean()
    sigma_hat <- (data %>% diff(2) %>% abs() %>% mean())/1.128
    ucl <- mu_hat + k * sigma_hat
    lcl <- mu_hat - k * sigma_hat
  }
    
  # 平均值偏移
  # delta <- 0 
  # mu1 <- mu + delta * sigma

  j <- 0
  for(i in data) {
    if(i > ucl | i < lcl) j <- j + 1
  }
  
  p <- j/m
  RL <- 1/p
  return(RL)
}
```

假設已知母體平均數與標準差，填入 mu 與 sigma 參數，利用母體平均數與標準差去模擬品管數據

```R
library(doParallel)

if(getDoParWorkers() == 1){
  cores <- makeCluster(detectCores())
  registerDoParallel(cores)
}

ARL_list <- foreach(1:1000, .combine = c, .packages = c("tidyverse")) %dopar% {
  sample_data <- rnorm(n = 20000, mean = 10, sd = 1)
  getRL(data = sample_data, mu = 10, sigma = 1)
}

mean(ARL_list)
median(ARL_list)

# stopCluster(cores)
```

由理論值可得知，一個管制圖正負三倍標準差的管制界限會得到一個 ARL 的值為

```R
1/0.0027 = 370.3704
```

由上述計算多個 ARL 之後得到

```R
median(ARL_list) # 370.3704
mean(ARL_list) # 377.5174
```

ps<sup>1</sup>. 約 1000 個迴圈之後中位數穩定於 370.3704 (可能有所不同)

ps<sup>2</sup>. 平均數、中位數隨 seed 而變動

假設未知母體平均數與標準差，不填入平均值與標準差的參數，函數會判斷計算上下限

```R
library(doParallel)

if(getDoParWorkers() == 1){
  cores <- makeCluster(detectCores())
  registerDoParallel(cores)
}

ARL_list <- foreach(1:1000, .combine = c, .packages = c("tidyverse")) %dopar% {
  sample_data <- rnorm(n = 20000, mean = 10, sd = 1)
  getRL(data = sample_data)
}

mean(ARL_list)
median(ARL_list)

# stopCluster(cores)
```

方法二

* alpha以第一次發生異常點計算
* 如出現 Inf 及代表樣本數不夠
* 以平均數較貼近理論值

```R
library(doParallel)
library(tidyverse)

getRL2 <- function(data, mu = NULL, sigma = NULL, k = 3, delta = 0){
  m <- length(data)
  
  if(!is.null(mu) & !is.null(sigma)) {
    ucl <- mu + k * sigma
    lcl <- mu - k * sigma
  } else if (is.null(mu) & !is.null(sigma)){
    stop("Missing mu")
  } else if (!is.null(mu) & is.null(sigma)){
    stop("Missing sigma")
  } else {
    mu_hat <- data %>% mean()
    sigma_hat <- (data %>% diff(2) %>% abs() %>% mean())/1.128
    ucl <- mu_hat + k * sigma_hat
    lcl <- mu_hat - k * sigma_hat
  }
    
  # 平均值偏移
  # delta <- 0 
  # mu1 <- mu + delta * sigma
  p <- 0
  
  for(i in 1:m){
    if(data[i] > ucl || data[i] < lcl) {
      p <- 1/i
      break
    }
  }

  RL <- 1/p
  return(RL)
}
```

假設已知母體平均數與標準差

```
library(doParallel)

if(getDoParWorkers() == 1){
  cores <- makeCluster(detectCores())
  registerDoParallel(cores)
}

ARL_list <- foreach(1:10000, .combine = c, .packages = c("tidyverse")) %dopar% {
  sample_data <- rnorm(n = 20000, mean = 10, sd = 1)
  getRL2(data = sample_data, mu = 10, sigma = 1)
}

mean(ARL_list)
median(ARL_list)

# stopCluster(cores)
```

假設未知母體平均數與標準差

```
library(doParallel)

if(getDoParWorkers() == 1){
  cores <- makeCluster(detectCores())
  registerDoParallel(cores)
}

ARL_list <- foreach(1:10000, .combine = c, .packages = c("tidyverse")) %dopar% {
  sample_data <- rnorm(n = 20000, mean = 10, sd = 1)
  getRL2(data = sample_data)
}

mean(ARL_list)
median(ARL_list)

# stopCluster(cores)
```

## 運用 bootstrap 模擬資料計算 ARL

方法一

* 偵測異常發生的機率

```R
library(doParallel)
library(tidyverse)

getRL_bootstrap <- function(data, indices, mu = NULL, sigma = NULL, k = 3, delta = 0){
  data <- data[indices]
  m <- length(data)
  
  if(!is.null(mu) & !is.null(sigma)) {
    ucl <- mu + k * sigma
    lcl <- mu - k * sigma
  } else if (is.null(mu) & !is.null(sigma)){
    stop("Missing mu")
  } else if (!is.null(mu) & is.null(sigma)){
    stop("Missing sigma")
  } else {
    mu_hat <- data %>% mean()
    sigma_hat <- (data %>% diff(2) %>% abs() %>% mean())/1.128
    ucl <- mu_hat + k * sigma_hat
    lcl <- mu_hat - k * sigma_hat
  }
    
  # 平均值偏移
  # delta <- 0 
  # mu1 <- mu + delta * sigma
  j <- 0
  for(i in data) {
    if(i > ucl | i < lcl) j <- j + 1
  }
  
  p <- j/m

  if(j != 0){
    RL <- 1/p
  } else {
    RL <- NULL
  }
  
  return(RL)
}

```

透過 `rnorm` 模擬 20000 個常態分配資料，bootstrap 會每次重新抽樣這 20000 筆數據

```{r}
set.seed(1)
sample_data <- rnorm(n = 20000, mean = 10, sd = 1)
b1 <- boot(data=sample_data, statistic = getRL_bootstrap, R = 5000, parallel = "multicore", ncpus = 28)
paste("the ARL mean is :", b1$t %>% mean())
paste("the ARL median is :", b1$t %>% median(na.rm = TRUE))
paste("the ARL mode is :", which.max(table(b1$t)))
```

> 當樣本資料 2000 筆時，計算 ARL 經常不穩定

* 可能會偵測不到異常數據

```{r}
set.seed(100)
sample_data <- rnorm(n = 2000, mean = 10, sd = 1)
b1 <- boot(data=sample_data, statistic = getRL_bootstrap, R = 2000, parallel = "multicore", ncpus = 28)
paste("the ARL mean is :", b1$t %>% mean(na.rm = TRUE))
paste("the ARL median is :", b1$t %>% median(na.rm = TRUE))
paste("the ARL mode is :", which.max(table(b1$t)))
```

方法二

* 偵測到第一筆發生錯誤時

```{r}
library(doParallel)
library(tidyverse)

getRL2_bootstrap <- function(data, indices, mu = NULL, sigma = NULL, k = 3, delta = 0){
  data <- data[indices]
  m <- length(data)
  
  if(!is.null(mu) & !is.null(sigma)) {
    ucl <- mu + k * sigma
    lcl <- mu - k * sigma
  } else if (is.null(mu) & !is.null(sigma)){
    stop("Missing mu")
  } else if (!is.null(mu) & is.null(sigma)){
    stop("Missing sigma")
  } else {
    mu_hat <- data %>% mean()
    sigma_hat <- (data %>% diff(2) %>% abs() %>% mean())/1.128
    ucl <- mu_hat + k * sigma_hat
    lcl <- mu_hat - k * sigma_hat
  }
    
  # 平均值偏移
  # delta <- 0 
  # mu1 <- mu + delta * sigma
  p <- 0
  
  for(i in 1:m){
    if(data[i] > ucl || data[i] < lcl) {
      p <- 1/i
      break
    }
  }

  if(p != 0){
    RL <- 1/p
  } else {
    RL <- NULL
  }
  
  return(RL)
}

```


```{r}
set.seed(1)
sample_data <- rnorm(n = 20000, mean = 10, sd = 1)
b1 <- boot(data=sample_data, statistic = getRL2_bootstrap, R = 5000, parallel = "multicore", ncpus = 28)
paste("the ARL mean is :", b1$t %>% mean())
paste("the ARL median is :", b1$t %>% median(na.rm = TRUE))
paste("the ARL mode is :", which.max(table(b1$t)))
```
