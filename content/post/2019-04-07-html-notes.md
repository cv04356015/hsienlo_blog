---
draft: false
layout: blog
title: HTML Notes
author: Hsien Ching Lo
date: 2019-04-07T07:25:03.434Z
categories:
  - HTML
tags:
  - HTML
Keywords:
  - HTML
---

# scrollbar

原始碼


```html
<div style="width:300px;height:250px;overflow:auto;">
文字內容
</div>
```

效果

<div style="width:300px;height:100px;overflow:auto;">
其中width和height為單元格的寬高
overflow-y:scroll; 出現上下的滾動
overflow-x:scroll; 出現左右的滾動
overflow：auto為自動，超出單元格就會自動增加拉吧
scrollbar-face-color(立體滾動條拉動色塊的顏色) 
scrollbar-highlight-color(滾動條空白部份的顏色) 
scrollbar-shadow-color(立體滾動條陰影的顏色) 
scrollbar-arrow-color(上下按鍵上三角箭頭的顏色) 
scrollbar-base-color(滾動條的基本顏色) 
scrollbar-dark-shadow-color(立體滾動條強陰影的顏色)
</div>

<!--more-->

```html
其中width和height為單元格的寬高
overflow-y:scroll; 出現上下的滾動
overflow-x:scroll; 出現左右的滾動
overflow：auto為自動，超出單元格就會自動增加拉吧
scrollbar-face-color(立體滾動條拉動色塊的顏色) 
scrollbar-highlight-color(滾動條空白部份的顏色) 
scrollbar-shadow-color(立體滾動條陰影的顏色) 
scrollbar-arrow-color(上下按鍵上三角箭頭的顏色) 
scrollbar-base-color(滾動條的基本顏色) 
scrollbar-dark-shadow-color(立體滾動條強陰影的顏色)
```
