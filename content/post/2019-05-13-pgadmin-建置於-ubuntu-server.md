---
draft: false
layout: blog
title: pgAdmin 建置於 Ubuntu Server
author: Hsien Ching Lo
date: 2019-05-13T08:46:44.212Z
categories:
  - PostgreSQL
  - pgAdmin
tags:
  - PostgreSQL
  - pgAdmin
Keywords:
  - PostgreSQL
  - pgAdmin
---
系統環境
* Ubuntu Server 18.04.2 LTS

1.下載更新 pgadmin4 的 docker

```bash
sudo docker pull dpage/pgadmin4
```

2.執行 docker

```bash
sudo docker run -p 8888:80 -d \
-v "/private/var/lib/pgadmin:/var/lib/pgadmin" \
-e "PGADMIN_DEFAULT_EMAIL=eic@cdc.gov.tw" \
-e "PGADMIN_DEFAULT_PASSWORD=1qaz@WSX" \
--name pgadmin4 \
dpage/pgadmin4
```

<!--more-->

3.加入服務(假設伺服器重啟後 pgadmin 循環錯誤)

pgadmin.sh

```
#!/bin/bash
sudo docker restart pgadmin4
```

新增權限與修改文件

```bah
sudo chmod +x /usr/sbin/pgadmin.sh
sudo vim /etc/systemd/system/pgadmin.service
```

```pgadmin.service
[Unit]
Description=pgAdmin
After=network.target

[Service]
User=root
Group=root
ExecStart=/usr/sbin/pgadmin.sh
Restart=always
WorkingDirectory=/

[Install]
WantedBy=multi-user.target
```
