---
draft: false
layout: blog
title: Jupyter Notebook Server 建置
author: Hsien Ching Lo
date: 2019-04-30T02:26:08.001Z
categories:
  - Jupyter
tags:
  - Jupyter
Keywords:
  - Jupyter
---
# Jupyter Notebook Server 安裝建置

---

## 前言

---

* anaconda 安裝在 root 下面，而不是使用者
* anaconda  安裝參考 [Anaconda 安裝建置](/jupyter-notebook-server-an-zhuang-jian-zhi/anaconda-an-zhuang-jian-zhi.md)

## configuration

---

```
# 開啟 server
jupyter notebook
# 新增 config 文件
jupyter notebook --generate-config
# 文件位置於 /root/.jupyter/jupyter_notebook_config.py
# 由於我將安裝位置安裝在 /opt/anaconda3 所以 config 不在使用者目錄下
root@rserver:/home# vim /root/.jupyter/jupyter_notebook_config.py
```

<!--more-->

* Automatic Password setup

```
$ python
```

```
# import the library
from notebook.auth import passwd

# generate SHA-1 password
# for exmaple: u'sha1:xxx:yyy'
passwd()
```

```
root@rserver:/home# python
Python 3.6.3 |Anaconda, Inc.| (default, Oct 13 2017, 12:02:49)
[GCC 7.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from notebook.auth import passwd
>>> passwd()
Enter password:
Verify password:
'sha1:4bcfc80b97ac:c53d2768d9e7a155f392ab1eb0af64b95bba7f6d'
```

* Running a public notebook server

```
# edit the configuration
$ vim /root/.jupyter/jupyter_notebook_config.py
```

```
# Set options for certfile, ip, password, and toggle off browser auto-opening
# if the certfile and keyfile is not set, jupyter notebook would be run over http protocol
# c.NotebookApp.certfile = u'/etc/letsencrypt/live/example.com/cert.pem'
# c.NotebookApp.keyfile = u'/etc/letsencrypt/live/example.com/privkey.pem'
# Set ip to '*' to bind on all interfaces (ips) for the public server
c.NotebookApp.ip = '*'
c.NotebookApp.password = u'sha1:4bcfc80b97ac:c53d2768d9e7a155f392ab1eb0af64b95bba7f6d'
c.NotebookApp.open_browser = False

# It is a good idea to set a known, fixed port for server access
c.NotebookApp.port = 8888

# the new url is http(s)://xxx:8888/jupyter
# [optional for proxy_pass] Running the notebook with a customized URL prefix
c.NotebookApp.base_url = '/jupyter/'
c.NotebookApp.webapp_settings = {'static_url_prefix':'/jupyter/static/'}
```

## Establish the service.

---

* Establish the execution script.

```
# create a shell script
$ vim /home/(user)/anaconda3/jupyternotebook.sh
```

內容.

```
#!/bin/bash
/home/(user)/anaconda3/bin/jupyter notebook
```

加權限.

```
# add execution property to the script
$ chmod a+x /home/(user)/anaconda3/jupyternotebook.sh
```

建立服務

```
# create a new service
$ sudo vim /etc/systemd/system/jupyternotebook.service
```

內容.

```
[Unit]
Description=jupyter notebook server
After=network.target

[Service]
User=(user)
Group=(user)
ExecStart=/home/(user)/anaconda3/jupyternotebook.sh
Restart=always
WorkingDirectory=/home/(user)/(path)

[Install]
WantedBy=multi-user.target
```

開機選項

```
sudo systemctl enable jupyternotebook.service
```

服務啟動

```
sudo systemctl start jupyternotebook.service
sudo systemctl status jupyternotebook.service
sudo systemctl enable jupyternotebook.service
```

Start the service.

```
<!-- surf the link -->
http(s)://example.com:8888/jupyter
```



