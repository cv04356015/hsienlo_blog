---
draft: true

title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
type: "post"        #分頁過濾 post
Keywords: [""]  
categories: []
tags: []

#以下為默認設置居多，可直接刪除
toc:                #默認開啟，可false關閉 
tocStartLevel: 2    #toc導航開始層次，最小為1
tocEndLevel: 4      #toc導航結束層次，最大為6
comment:        #默認開啟，可false關閉 
author: ""
CopyrightStatement: ""  #文章版權說明，一般默認為原創

# 內容與---空一行以便與 wiz 筆記markdown渲染兼容
---

**Markdown**  提示內容

<!--more-->


### 參考資料


### 版本信息

|Version|Action |Time |
|:--:|:--:|:--:|
|1.0|init|{{ dateFormat "2006-01-02" .Date}}|
 

 